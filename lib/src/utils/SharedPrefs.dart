import 'dart:convert';

import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  //Set/Get User
  static void saveUser(UserModel model) async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    dynamic userResponse = model.toJson();
    String jsonString = jsonEncode(userResponse);
    sharedUser.setString('user', jsonString);
  }

  static Future<UserModel> getUser() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap = json.decode(sharedUser.getString('user'));
    var user = UserModel.fromJson(userMap);
    return user;
  }

  static void removeUser() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    sharedUser.remove('user');
  }

  static void setUserLoggedIn(bool loggedIn) async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    sharedUser.setBool('isLoggedIn', loggedIn);
  }

  static Future<bool> isUserLoggedIn() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    return sharedUser.getBool('isLoggedIn') ?? false;
  }

  static void setRememberMe(String email, String pwd) async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    sharedUser.setBool('isRemember', true);
    sharedUser.setString('email', email);
    sharedUser.setString('pwd', pwd);
  }

  static Future<RememberModel> getRememberMe() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    return RememberModel(sharedUser.getBool('isRemember') ?? false,
        sharedUser.get('email') ?? "", sharedUser.get('pwd') ?? "");
  }

  static void removeRememberMe() async {
    SharedPreferences sharedUser = await SharedPreferences.getInstance();
    sharedUser.remove('isRemember');
    sharedUser.remove('email');
  }
}
