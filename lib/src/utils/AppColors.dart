import 'dart:ui';

class AppColors {

  static final Color appThemeColor = Color(0xFFC3002F);
  static final Color labelGrayColor = Color(0xFF8E8E8E);
  static final Color dividerLineColor = Color(0xFFF5F5F5);
  static final Color darkDividerLineColor = Color(0xFFD3D3D3);
  static final Color lightGrayBgColor = Color(0xFFF2F2F2);
  static final Color darkGrayBgColor = Color(0xFFE2E1E1);
  static final Color yellowProgressColor = Color(0xFFF19C09);
  static final Color greenToggleColor = Color(0xFF68B26A);
}