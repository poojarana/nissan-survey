
class AppConstants {

  //Validation Messages
  static const String noConnection = 'Internet Connection not avaliable';
  static const String enterEmail= 'Please enter email';
  static const String emailValidation= 'Please enter valid email';
  static const String enterPassword= 'Please enter password';
  static const String enterNewPassword= 'Please enter new password';
  static const String confirmPassword= 'Please confirm password';
  static const String passwordNotMatch= 'New password and confirm password does not match';

  static const String addImage= 'Please add some images';
  static const String enterComment= 'Please enter comment';
  static const String pdfDownloadSuccess= 'PDF file downloaded successfully.';
  static const String pdfDownloadFailed= 'Failed to download PDF file.';

  static const String S3BucketUrl = 'https://nissansurveyapp.s3.amazonaws.com/survey/';
  static const String S3BucketPackageUrl = 'https://s3-us-east-1.amazonaws.com/nissansurveyapp/survey/';

  static bool isLoggedIn = false;
  static bool isLaunching = true; //POST
  static String isReviewed = "0";

}