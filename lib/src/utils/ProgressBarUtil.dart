import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'AppColors.dart';

class ProgressBarUtil {
  static showLoader(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Padding(
              padding: EdgeInsets.only(top: 100),
              child: Center(
                child: CircularProgressIndicator(
                    backgroundColor: AppColors.appThemeColor,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black26)),
              ));
        });
  }

  static ProgressDialog pr;
  static void showProgressDialog(BuildContext context) {
    //For normal dialog
    if (pr != null && pr.isShowing()) {
      pr.hide();
    }
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(message: 'Please wait...');
    pr.show();
  }

  static void hideProgressDialog(BuildContext context) {
    //For normal dialog
    if (pr != null && pr.isShowing()) {
      pr.hide();
    }
  }
}
