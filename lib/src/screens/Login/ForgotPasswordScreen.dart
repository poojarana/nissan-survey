import 'package:flutter/material.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  ForgotPasswordState createState() => ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPasswordScreen> {
  TextEditingController emailTxt = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Stack(children: <Widget>[
                Positioned.fill(
                  child: Image.asset(
                    "assets/graphic-bg.png",
                    fit: BoxFit.contain,
                    alignment: Alignment.topCenter,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 100),
                        Center(
                            child: Image.asset("assets/nissan-logo-inner.png")),
                        SizedBox(height: 70),
                        Text("Email",
                            style: TextStyle(
                                color: AppColors.labelGrayColor,
                                fontWeight: FontWeight.w400)),
                        addTextField(),
                        SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            addBottomButton("BACK", () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              Navigator.of(context).pop();
                            }),
                            addBottomButton("FORGOT PASSWORD", () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              if (emailTxt.text.trim().isEmpty) {
                                AppUtil.showToast(
                                    AppConstants.enterEmail, false);
                              } else if (!AppUtil.validateEmail(
                                  emailTxt.text.trim())) {
                                AppUtil.showToast(
                                    AppConstants.emailValidation, false);
                              } else {
                                forgotPasswordApiCall();
                              }
                            })
                          ],
                        )
                      ]),
                ),
              ]))),
    );
  }

  Widget addTextField() {
    return Padding(
        padding: EdgeInsets.only(top: 5),
        child: Theme(
          data:
              Theme.of(context).copyWith(primaryColor: AppColors.appThemeColor),
          child: TextField(
            controller: emailTxt,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.w400),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(bottom: 10.0),
              isDense: true,
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
            ),
            minLines: 1,
            maxLines: 1,
          ),
        ));
  }

  Widget addBottomButton(String buttonTitle, VoidCallback buttonAction) {
    double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
    return GestureDetector(
        onTap: () {
          buttonAction();
        },
        child: Container(
          height: 50,
          width: buttonWidth,
          decoration: new BoxDecoration(
            color: AppColors.appThemeColor,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(
              const Radius.circular(25.0),
            ),
          ),
          child: Center(
            child: Text(
              buttonTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ));
  }

  void forgotPasswordApiCall() {
    AppUtil.checkInternet().then((value) {
      if (value) {
        ProgressBarUtil.showLoader(context);
        ApiController.forgotPasswordApi(emailTxt.text).then((responseModel) {
          Navigator.of(context).pop();
          if (responseModel.success) {
            Navigator.of(context).pop();
          } else {
            AppUtil.showToast(responseModel.message, false);
          }
        });
      } else {
        AppUtil.showToast(AppConstants.noConnection, false);
      }
    });
  }
}
