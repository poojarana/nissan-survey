import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/SideMenu/SideMenu.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';
import 'ForgotPasswordScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetFloat;

  TextEditingController emailTxt = TextEditingController();
  TextEditingController passwordTxt = TextEditingController();
  bool isRememberMeSelected = false;

  @override
  void initState() {
    super.initState();
    checkIfRemembered();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    _offsetFloat = Tween<Offset>(begin: Offset(4.0, 0.0), end: Offset.zero)
        .animate(_controller);

    _controller.forward();
  }

  void checkIfRemembered() async {
    RememberModel model = await SharedPrefs.getRememberMe();
    isRememberMeSelected = model.isRemember;
    emailTxt = TextEditingController(text: model.email);
    passwordTxt = TextEditingController(text: model.password);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: WillPopScope(
            child: SingleChildScrollView(
                child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                    },
                    child: Stack(children: <Widget>[
                      Positioned.fill(
                        child: Image.asset(
                          "assets/graphic-bg.png",
                          fit: BoxFit.contain,
                          alignment: Alignment.topCenter,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(height: 100),
                              Center(
                                  child: Image.asset(
                                      "assets/nissan-logo-inner.png")),
                              SizedBox(height: 50),
                              SlideTransition(
                                position: _offsetFloat,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Email",
                                        style: TextStyle(
                                            color: AppColors.labelGrayColor,
                                            fontWeight: FontWeight.w400)),
                                    addTextField(emailTxt, false),
                                    SizedBox(height: 30),
                                    Text("Password",
                                        style: TextStyle(
                                            color: AppColors.labelGrayColor,
                                            fontWeight: FontWeight.w400)),
                                    addTextField(passwordTxt, true),
                                    SizedBox(height: 25),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                              onTap: () {
                                                FocusScope.of(context)
                                                    .requestFocus(FocusNode());
                                                setState(() {
                                                  isRememberMeSelected =
                                                      !isRememberMeSelected;
                                                });
                                              },
                                              child: Row(children: [
                                                Image.asset(isRememberMeSelected
                                                    ? 'assets/rememberSelected.png'
                                                    : 'assets/rememberUnSelected.png'),
                                                SizedBox(width: 10),
                                                Text("Remember me",
                                                    style: TextStyle(
                                                        color: AppColors
                                                            .labelGrayColor,
                                                        fontWeight:
                                                            FontWeight.w400))
                                              ])),
                                          InkWell(
                                            onTap: () {
                                              FocusScope.of(context)
                                                  .requestFocus(FocusNode());
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ForgotPasswordScreen()));
                                            },
                                            child: Text("Forgot your Password?",
                                                style: TextStyle(
                                                    color:
                                                        AppColors.appThemeColor,
                                                    fontWeight:
                                                        FontWeight.w400)),
                                          )
                                        ]),
                                    SizedBox(height: 40),
                                    addLoginButton(),
                                    SizedBox(height: 20),
                                  ],
                                ),
                              ),
                            ]),
                      )
                    ]))),
            onWillPop: onWillPop));
  }

  Future<bool> onWillPop() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Are you sure?'),
          content: Text('Do you want to exit an App'),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    ) ?? false;
  }

  Widget addTextField(TextEditingController controller, bool isPassword) {
    return Padding(
        padding: EdgeInsets.only(top: 5),
        child: Theme(
          data:
              Theme.of(context).copyWith(primaryColor: AppColors.appThemeColor),
          child: TextField(
            controller: controller,
            obscureText: isPassword,
            keyboardType:
                isPassword ? TextInputType.text : TextInputType.emailAddress,
            style: TextStyle(
                fontSize: 16.0,
                color: isPassword ? AppColors.appThemeColor : Colors.black,
                fontWeight: FontWeight.w400),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(bottom: 10.0),
              isDense: true,
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
            ),
            minLines: 1,
            maxLines: 1,
          ),
        ));
  }

  Widget addLoginButton() {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (emailTxt.text.trim().isEmpty) {
            AppUtil.showToast(AppConstants.enterEmail, false);
          } else if (passwordTxt.text.trim().isEmpty) {
            AppUtil.showToast(AppConstants.enterPassword, false);
          } else if (!AppUtil.validateEmail(emailTxt.text.trim())) {
            AppUtil.showToast(AppConstants.emailValidation, false);
          } else {
            loginApiCall();
          }
        },
        child: Container(
          height: 50,
          decoration: new BoxDecoration(
            color: AppColors.appThemeColor,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(
              const Radius.circular(25.0),
            ),
          ),
          child: Center(
            child: Text(
              "LOGIN",
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ));
  }

  void loginApiCall() {
    AppUtil.checkInternet().then((value) {
      if (value) {
        ProgressBarUtil.showLoader(context);
        ApiController.loginApi(emailTxt.text, passwordTxt.text)
            .then((userModel) {
          Navigator.of(context).pop();
          if (userModel.success) {
            SharedPrefs.removeRememberMe();
            if (isRememberMeSelected) {
              SharedPrefs.setRememberMe(emailTxt.text, passwordTxt.text);
            }
            SharedPrefs.saveUser(userModel);
            SharedPrefs.setUserLoggedIn(true);
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => SideMenuPage()),
                (Route<dynamic> route) => false);
          } else {
            AppUtil.showToast(userModel.message, false);
          }
        });
      } else {
        AppUtil.showToast(AppConstants.noConnection, false);
      }
    });
  }
}
