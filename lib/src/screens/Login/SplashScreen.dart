import 'dart:async';
import 'package:flutter/material.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/screens/SideMenu/SideMenu.dart';
import 'package:nissan_survey/src/screens/Login/LoginScreen.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<SplashScreen> with TickerProviderStateMixin {
  Timer _timer;
  Animation<double> animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();

    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    animation = Tween<double>(begin: 0, end: -150).animate(animationController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed)
          Navigator.of(context).pushReplacement(CustomPageRoute(
              AppConstants.isLoggedIn ? SideMenuPage() : LoginScreen()));
      });
    startTimer();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer(oneSec, () {
      animationController.forward();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: AppColors.appThemeColor,
      child: Transform.translate(
        offset: Offset(0, animation.value),
        child: Center(child: Image.asset("assets/nissan_splash.png")),
      ),
    ));
  }
}
