import 'package:flutter/material.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePasswordScreen> {
  TextEditingController oldPasswordTxt = TextEditingController();
  bool showOldTxt = true;

  TextEditingController newPasswordTxt = TextEditingController();
  bool showNewTxt = true;

  TextEditingController confirmPasswordTxt = TextEditingController();
  bool showConfirmTxt = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: Image.asset(
                      "assets/graphic-bg.png",
                      fit: BoxFit.contain,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 150),
                          Text("Old Password",
                              style: TextStyle(
                                  color: AppColors.labelGrayColor,
                                  fontWeight: FontWeight.w400)),
                          addTextField(oldPasswordTxt, 0),
                          SizedBox(height: 30),
                          Text("New Password",
                              style: TextStyle(
                                  color: AppColors.labelGrayColor,
                                  fontWeight: FontWeight.w400)),
                          addTextField(newPasswordTxt, 1),
                          SizedBox(height: 30),
                          Text("Confirm Password",
                              style: TextStyle(
                                  color: AppColors.labelGrayColor,
                                  fontWeight: FontWeight.w400)),
                          addTextField(confirmPasswordTxt, 2),
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              addBottomButton("BACK", () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                Navigator.of(context).pop();
                              }),
                              addBottomButton("CHANGE PASSWORD", () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                if (oldPasswordTxt.text.trim().isEmpty) {
                                  AppUtil.showToast(
                                      AppConstants.enterPassword, false);
                                } else if (newPasswordTxt.text.trim().isEmpty) {
                                  AppUtil.showToast(
                                      AppConstants.enterNewPassword, false);
                                } else if (confirmPasswordTxt.text
                                    .trim()
                                    .isEmpty) {
                                  AppUtil.showToast(
                                      AppConstants.confirmPassword, false);
                                } else if (newPasswordTxt.text.trim() !=
                                    confirmPasswordTxt.text.trim()) {
                                  AppUtil.showToast(
                                      AppConstants.passwordNotMatch, false);
                                } else {
                                  changePasswordApiCall();
                                }
                              })
                            ],
                          )
                        ]),
                  )
                ],
              ))),
    );
  }

  Widget addTextField(TextEditingController controller, int fieldType) {
    return Padding(
        padding: EdgeInsets.only(top: 5),
        child: Theme(
          data:
              Theme.of(context).copyWith(primaryColor: AppColors.appThemeColor),
          child: TextField(
            controller: controller,
            keyboardType: TextInputType.text,
            obscureText: fieldType == 0
                ? showOldTxt
                : fieldType == 1 ? showNewTxt : showConfirmTxt,
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.w400),
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.appThemeColor),
              ),
              suffixIcon: IconButton(
                icon: Icon(
                  // Based on passwordVisible state choose the icon
                  fieldType == 0
                      ? showOldTxt ? Icons.visibility : Icons.visibility_off
                      : fieldType == 1
                          ? showNewTxt ? Icons.visibility : Icons.visibility_off
                          : showConfirmTxt
                              ? Icons.visibility
                              : Icons.visibility_off,
                  color: AppColors.appThemeColor,
                ),
                onPressed: () {
                  // Update the state i.e. toggle the state of passwordVisible variable
                  fieldType == 0
                      ? showOldTxt = !showOldTxt
                      : fieldType == 1
                          ? showNewTxt = !showNewTxt
                          : showConfirmTxt = !showConfirmTxt;
                  setState(() {});
                },
              ),
            ),
            minLines: 1,
            maxLines: 1,
          ),
        ));
  }

  Widget addBottomButton(String buttonTitle, VoidCallback buttonAction) {
    double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
    return GestureDetector(
        onTap: () {
          buttonAction();
        },
        child: Container(
          height: 50,
          width: buttonWidth,
          decoration: new BoxDecoration(
            color: AppColors.appThemeColor,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(
              const Radius.circular(25.0),
            ),
          ),
          child: Center(
            child: Text(
              buttonTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ));
  }

  void changePasswordApiCall() {
    AppUtil.checkInternet().then((value) {
      if (value) {
        ProgressBarUtil.showLoader(context);
        ApiController.changePasswordApi(
                oldPasswordTxt.text, newPasswordTxt.text)
            .then((responseModel) {
          Navigator.of(context).pop();
          if (responseModel.success) {
            Navigator.of(context).pop();
          } else {
            AppUtil.showToast(responseModel.message, false);
          }
        });
      } else {
        AppUtil.showToast(AppConstants.noConnection, false);
      }
    });
  }
}
