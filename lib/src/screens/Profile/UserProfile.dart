import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'ChangePasswordScreen.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfileScreen> {
  UserModel model;
  String imagePath;

  @override
  void initState() {
    super.initState();
    getUser();
  }

  void getUser() async {
    model = await SharedPrefs.getUser();
    if(model.profilePicture != null && model.profilePicture != "") {
      final directory = await getApplicationDocumentsDirectory();
      imagePath = '${directory.path}/profileImage.png';
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.appThemeColor,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        Container(
          height: 250,
          color: AppColors.appThemeColor,
          child: Column(children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: InkWell(
                      child: Text("Cancel",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w300)),
                      onTap: () {
                        final ScaffoldState scaffoldState =
                            context.findRootAncestorStateOfType();
                        scaffoldState.openDrawer();
                      })),
              Text("Settings",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w400)),
              Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: InkWell(
                      child: Text("Done",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w300)),
                      onTap: () {
                        final ScaffoldState scaffoldState =
                            context.findRootAncestorStateOfType();
                        scaffoldState.openDrawer();
                      }))
            ]),
            Padding(
                padding: EdgeInsets.only(top: 55),
                child: CircleAvatar(
                    radius: 65,
                    backgroundImage: imagePath == null
                            ? AssetImage('assets/defaultUser.png')
                            : FileImage(File(imagePath))))
          ]),
        ),
        Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 40),
              Text("General Settings",
                  style: TextStyle(
                      color: AppColors.labelGrayColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Name",
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.w400)),
                  Text(model != null ? model.fullName : "",
                      style: TextStyle(
                          color: AppColors.labelGrayColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w400))
                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Email",
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.w400)),
                  Text(model != null ? model.email : "",
                      style: TextStyle(
                          color: AppColors.labelGrayColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w400))
                ],
              ),
              SizedBox(height: 40),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ChangePasswordScreen()));
                },
                child: Text("Change Password",
                    style: TextStyle(
                        color: AppColors.appThemeColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w400)),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 40, bottom: 40),
                  child: Container(
                      height: 1.5,
                      width: MediaQuery.of(context).size.width - 40,
                      color: AppColors.darkDividerLineColor)),
              Text("Information",
                  style: TextStyle(
                      color: AppColors.labelGrayColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 20),
              Text("FAQ",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 16,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 20),
              Text("Terms & Conditions",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 16,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 20),
              Text("Contact Us",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 16,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 20),
            ])),
      ])),
    );
  }
}
