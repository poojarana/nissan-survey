import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/widgets/SubmissionPopup.dart';
import 'QuestionnairePage.dart';
import 'ReviewScreen.dart';

class SurveyQuestionnaireScreen extends StatefulWidget {
  final Department department;

  SurveyQuestionnaireScreen(this.department);
  @override
  _SurveyQuestionnaireState createState() => _SurveyQuestionnaireState();
}

class _SurveyQuestionnaireState extends State<SurveyQuestionnaireScreen> {
  Future<QuestionAnswerModel> response;

  PageController _controller;
  var currentPageValue = 0;

  @override
  void initState() {
    super.initState();
    response = ApiController.getDepartmentInfoApi(widget.department.id);
    _controller = new PageController()..addListener(_listener);
  }

  _listener() {
    setState(() {
      currentPageValue = _controller.page.round();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage('assets/graphic-bg-inner.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ),
            child: FutureBuilder(
                future: response,
                builder: (context, projectSnap) {
                  if (projectSnap.connectionState == ConnectionState.none &&
                      projectSnap.hasData == null) {
                    return Container();
                  } else {
                    if (projectSnap.hasData) {
                      QuestionAnswerModel model = projectSnap.data;

                      if (model.success) {
                        if (model.questionArea.length > 0) {
                          return PageView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: model.questionArea.length,
                              controller: _controller,
                              itemBuilder: (context, index) {
                                return QuestionnairePage(
                                    PageInfo(widget.department, index + 1,
                                        model.questionArea.length),
                                    model.questionArea[index], (action) {
                                  List<Question> notSelectedAnswers = model
                                      .questionArea[index].questions
                                      .where(
                                          (question) => question.selection == 0)
                                      .toList();
                                  switch (action) {
                                    case PageAction.back:
                                      currentPageValue = index - 1;
                                      _controller.animateToPage(
                                          currentPageValue,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.ease);
                                      break;
                                    case PageAction.next:
                                      if (notSelectedAnswers.length == 0) {
                                        currentPageValue = index + 1;
                                        _controller.animateToPage(
                                            currentPageValue,
                                            duration:
                                                Duration(milliseconds: 500),
                                            curve: Curves.ease);
                                      } else {
                                        showAlertDialog();
                                      }
                                      break;
                                    case PageAction.review:
                                      if (notSelectedAnswers.length == 0) {
                                        openReviewPage(model.questionArea);
                                      } else {
                                        showAlertDialog();
                                      }
                                      break;
                                    case PageAction.submit:
                                      openSubmitPopup(model.questionArea);
                                      break;
                                  }
                                });
                              });
                        } else {
                          return justShowHeader(true);
                        }
                      } else {
                        return justShowHeader(true);
                      }
                    } else {
                      return justShowHeader(false);
                    }
                  }
                }),
          )),
    );
  }

  Widget justShowHeader(bool onlyHeader) {
    return Column(children: [
      AppHeaderView.addAppHeader(
          "assets/back-arrow.png",
          widget.department.selectedCity.selectedBranch.name,
          () => Navigator.of(context).pop()),
      SizedBox(height: 100),
      onlyHeader
          ? Container()
          : Center(
              child: CircularProgressIndicator(
                  backgroundColor: AppColors.appThemeColor,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black26)),
            )
    ]);
  }

  void openReviewPage(List<QuestionArea> questionAreas) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            ReviewScreen(widget.department, questionAreas, null, () {
              currentPageValue = 0;
              _controller.animateToPage(currentPageValue,
                  duration: Duration(milliseconds: 500), curve: Curves.ease);
            })));
  }

  void openSubmitPopup(List<QuestionArea> questionAreas) {
    showDialog<void>(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return SubmissionPopup(widget.department, questionAreas);
      },
    );
  }

  void showAlertDialog() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Nissan Survey"),
              content: Text("Please answer all options if you want to proceed."),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }
}
