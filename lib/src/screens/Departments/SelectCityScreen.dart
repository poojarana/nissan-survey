import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/screens/BestPractices/MarketPracticesScreen.dart';
import 'package:nissan_survey/src/screens/Departments/SelectBranchScreen.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';

class SelectCityScreen extends StatefulWidget {
  final String headerTitle;
  final Department department;
  SelectCityScreen(this.headerTitle, this.department);

  @override
  _SelectCityState createState() => _SelectCityState();
}

class _SelectCityState extends State<SelectCityScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader("assets/back-arrow.png", "Select Location",
            () => Navigator.of(context).pop()),
        Expanded(
            child: ListView.builder(
                //shrinkWrap: true,
                itemCount: widget.department.cities.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      widget.department.selectedCity =
                          widget.department.cities[index];
                      if (widget.department.parent == ParentInfo.viewBestPractice ||
                          widget.department.parent == ParentInfo.createBestPractice) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MarketPracticesScreen(widget.department)));
                      } else {
                        //Open select branch
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SelectBranchScreen(
                              widget.headerTitle, widget.department),
                        ));
                      }
                    },
                    child: Column(children: [
                      addCityRow(index),
                      Container(
                        height: 1,
                        color: AppColors.dividerLineColor,
                      )
                    ]),
                  );
                }))
      ]),
    ));
  }

  Widget addCityRow(int index) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(left: 30),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(widget.department.cities[index].name,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
        ),
      ),
    );
  }
}
