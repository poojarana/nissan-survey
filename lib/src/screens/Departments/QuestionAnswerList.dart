import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/widgets/ImageViewer.dart';
import 'package:nissan_survey/src/widgets/PhotoCapturePopup.dart';

class QuestionAnswerList extends StatefulWidget {
  final bool isEditable;
  final int pageNum;
  final QuestionArea questionArea;
  QuestionAnswerList(this.isEditable, this.pageNum, this.questionArea);

  @override
  _QuestionAnswerListState createState() => _QuestionAnswerListState();
}

class _QuestionAnswerListState extends State<QuestionAnswerList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 40),
        child: Container(
          width: MediaQuery.of(context).size.width - 20,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Color(0xFFD3D3D3),
                  blurRadius: 18.5,
                  offset: Offset(0.0, 8.0))
            ],
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Align(
                alignment: Alignment.centerRight,
                child: Padding(
                    padding: EdgeInsets.only(right: 30, bottom: 10),
                    child: Container(
                      height: 35,
                      width: 30,
                      decoration: BoxDecoration(
                          color: AppColors.appThemeColor,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(3.0),
                              bottomRight: Radius.circular(3.0))),
                      child: Center(
                          child: Text(widget.pageNum.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400))),
                    ))),
            Padding(
              padding: EdgeInsets.only(left: 84, bottom: 20),
              child: Text(widget.questionArea.area,
                  style: TextStyle(
                      color: AppColors.appThemeColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w500)),
            ),
            addQuestionAnswerCard(),
            widget.isEditable ? addCommentView() : enteredCommentView(),
            widget.questionArea.images.length > 0
                ? widget.isEditable
                    ? addCircularImageListView()
                    : createUploadedImagesListView()
                : Container(),
            widget.isEditable ? addCameraView() : Container()
          ]),
        ));
  }

  Widget addQuestionAnswerCard() {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.questionArea.questions.length,
            itemBuilder: (context, index) {
              Question question = widget.questionArea.questions[index];
              return Column(children: [
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  child: Row(children: [
//                    Transform.scale(
//                        scale: 1.5,
//                        child:
//                        Switch(
//                          value: question.isInitialAppearance
//                              ? false
//                              : question.isSwitched,
//                          onChanged: (value) {
//                            if (widget.isEditable) {
//                              question.isInitialAppearance = false;
//                              question.isSwitched = value;
//                              setState(() {});
//                            }
//                          },
//                          activeColor: Colors.white,
//                          activeTrackColor: AppColors.greenToggleColor,
//                          inactiveTrackColor: question.isInitialAppearance
//                              ? Colors.grey
//                              : AppColors.appThemeColor,
//                        )),
                    InkWell(
                        onTap: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (widget.isEditable) {
                            setState(() {
                              question.selection < 2
                                  ? question.selection += 1
                                  : question.selection = 1;
                            });
                          }
                        },
                        child: Image.asset(question.selection == 0
                            ? "assets/neutral.png"
                            : question.selection == 1
                                ? "assets/yes.png"
                                : "assets/no.png")),
                    SizedBox(width: 14),
                    Flexible(
                      child: Text(question.name ?? "",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w400)),
                    )
                  ]),
                ),
                Container(
                  height: 1,
                  color: AppColors.darkDividerLineColor,
                ),
                SizedBox(height: 20),
              ]);
            }));
  }

  Widget addCommentView() {
    return Padding(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
        child: TextField(
            controller: widget.questionArea.comment,
            keyboardType: TextInputType.multiline,
            style: TextStyle(fontWeight: FontWeight.w400),
            minLines: 5, //Normal textInputField will be displayed
            maxLines: 5, //
            maxLength: 150, // when user presses enter it will adapt to it
            decoration: InputDecoration(
              labelText: "Enter Your Comments",
              labelStyle: TextStyle(
                  color: AppColors.labelGrayColor,
                  fontSize:
                      15.0, //I believe the size difference here is 6.0 to account padding
                  fontWeight: FontWeight.w400),
              alignLabelWithHint: true,
              contentPadding: const EdgeInsets.all(0.0),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.dividerLineColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: AppColors.dividerLineColor),
              ),
            )));
  }

  Widget enteredCommentView() {
    String comment = widget.questionArea.addedComment != null &&
            widget.questionArea.addedComment != ""
        ? widget.questionArea.addedComment
        : widget.questionArea.comment.text;
    return comment == ""
        ? Container()
        : Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Comments",
                  style: TextStyle(
                      color: AppColors.labelGrayColor,
                      fontSize: 15,
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 10),
              Text(
                comment,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              )
            ]));
  }

  Widget addCameraView() {
    return InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        showDialog<void>(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context) {
            return PhotoCapturePopup((image) {
              ImageModel model = ImageModel();
              model.localImage = image;
              model.imageName = DateTime.now().toIso8601String();
              widget.questionArea.images.add(model);
              setState(() {});
            });
          },
        );
      },
      child: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Image.asset("assets/take-photo-icon.png"),
        Padding(
            padding: EdgeInsets.only(top: 10, bottom: 15),
            child: Text("Add Pictures",
                style: TextStyle(
                    color: AppColors.labelGrayColor,
                    fontSize: 13,
                    fontWeight: FontWeight.w400)))
      ])),
    );
  }

  Widget addCircularImageListView() {
    return Container(
        height: 80,
        padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: widget.questionArea.images.length,
            itemBuilder: (context, index) {
              ImageModel model = widget.questionArea.images[index];
              return Padding(
                  padding: EdgeInsets.only(right: 12.0),
                  child: Stack(children: [
                    Padding(
                        padding: EdgeInsets.only(right: 8.0),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            image: DecorationImage(
                              image: FileImage(model.localImage),
                              fit: BoxFit.fill,
                            ),
                          ),
                        )),
                    Positioned(
                        right: 0,
                        child: InkWell(
                            onTap: () {
                              setState(() {
                                widget.questionArea.images.remove(model);
                              });
                            },
                            child: Image.asset(
                              "assets/cross-icon.png",
                              width: 20,
                              height: 20,
                            )))
                  ]));
            }));
  }

  Widget createUploadedImagesListView() {
    return Container(
        height: 150,
        padding: EdgeInsets.only(bottom: 40),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: widget.questionArea.images.length,
            itemBuilder: (context, index) {
              return Padding(
                  padding: EdgeInsets.only(right: 8.0),
                  child: Container(
                    width: 150,
                    height: 100,
                    child: widget.questionArea.images[index].localImage == null
                        ? InkWell(
                            onTap: () {
                              openImageViewPopup(
                                  widget.questionArea.images[index].imageUrl);
                            },
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl:
                                  widget.questionArea.images[index].imageUrl,
                              placeholder: (context, url) => Center(
                                child: CircularProgressIndicator(
                                    backgroundColor: Colors.black26,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.black26)),
                              ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ))
                        : FadeInImage(
                            placeholder: AssetImage("assets/Grey.png"),
                            fit: BoxFit.cover,
                            image: FileImage(
                                widget.questionArea.images[index].localImage),
                          ),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: AppColors.darkDividerLineColor, width: 1),
                    ),
                  ));
            }));
  }

  void openImageViewPopup(String imageUrl) async {
    final cache = DefaultCacheManager();
    final file = await cache.getSingleFile(imageUrl);
    showDialog<void>(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return ImageViewerPopup(file);
      },
    );
  }
}
