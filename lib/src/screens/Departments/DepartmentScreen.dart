import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/BestPractices/GuidelinesDetailScreen.dart';
import 'package:nissan_survey/src/screens/BestPractices/MarketPracticesScreen.dart';
import 'package:nissan_survey/src/screens/Departments/SelectCityScreen.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'SelectBranchScreen.dart';

class DepartmentScreen extends StatefulWidget {
  final ParentInfo parent;
  final String title;

  DepartmentScreen(this.parent, this.title);
  @override
  _DepartmentScreenState createState() => _DepartmentScreenState();
}

class _DepartmentScreenState extends State<DepartmentScreen> {
  Future<DepartmentModel> response;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    response = ApiController.getDepartmentsApi("1");
  }

  _onLayoutDone(_) {
    //your logic here
    if (AppConstants.isLaunching) {
      final ScaffoldState scaffoldState = context.findRootAncestorStateOfType();
      scaffoldState.openDrawer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader(
            widget.parent == ParentInfo.department
                ? "assets/MENU.png"
                : "assets/back-arrow.png",
            widget.title, () {
          if (widget.parent == ParentInfo.department) {
            final ScaffoldState scaffoldState =
                context.findRootAncestorStateOfType();
            scaffoldState.openDrawer();
          } else {
            Navigator.of(context).pop();
          }
        }),
        FutureBuilder(
          future: response,
          builder: (context, projectSnap) {
            if (projectSnap.connectionState == ConnectionState.none &&
                projectSnap.hasData == null) {
              return Container();
            } else {
              if (projectSnap.hasData) {
                DepartmentModel model = projectSnap.data;

                if (model.success) {
                  return Expanded(
                      child: ListView.builder(
                          itemCount: model.departments.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Department selDepartment =
                                    model.departments[index];
                                selDepartment.parent = widget.parent;
                                if (widget.parent ==
                                        ParentInfo.viewBestPractice ||
                                    widget.parent ==
                                        ParentInfo.createBestPractice) {
                                  openBestPractices(selDepartment);
                                } else if (widget.parent ==
                                    ParentInfo.viewGuidelines) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          GuidelinesDetailScreen(
                                              selDepartment)));
                                } else {
                                  openCityBranch(selDepartment);
                                }
                              },
                              child: Column(children: [
                                addDepartmentRow(model.departments[index]),
                                Container(
                                  height: 1,
                                  color: AppColors.dividerLineColor,
                                )
                              ]),
                            );
                          }));
                } else {
                  return Container();
                }
              } else {
                return Padding(
                    padding: EdgeInsets.only(top: 100),
                    child: Center(
                      child: CircularProgressIndicator(
                          backgroundColor: AppColors.appThemeColor,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.black26)),
                    ));
              }
            }
          },
        )
      ]),
    ));
  }

  Widget addDepartmentRow(Department department) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      color: Colors.white,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Padding(
            padding: EdgeInsets.only(left: 30),
            child: Text(department.name.toUpperCase(),
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500))),
        Padding(
            padding: EdgeInsets.only(right: 30),
            child: Image.asset("assets/arrow.png"))
      ]),
    );
  }

  void openBestPractices(Department selDepartment) async {
    if (selDepartment.cities.length == 1) {
      selDepartment.selectedCity = selDepartment.cities[0];
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => MarketPracticesScreen(selDepartment)));
    } else {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SelectCityScreen("", selDepartment),
      ));
    }
  }

  void openCityBranch(Department selDepartment) {
    String headerTitle = widget.parent == ParentInfo.department
        ? "Select Branch in "
        : widget.parent == ParentInfo.mySurvey
            ? widget.title
            : "Select Location";
    if (selDepartment.cities.length == 1) {
      selDepartment.selectedCity = selDepartment.cities[0];
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SelectBranchScreen(headerTitle, selDepartment),
      ));
    } else {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SelectCityScreen(headerTitle, selDepartment),
      ));
    }
  }
}
