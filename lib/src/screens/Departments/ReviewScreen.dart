import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/network/NetworkApiCallUtil.dart';
import 'package:nissan_survey/src/network/NetworkConstants.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';
import 'package:share/share.dart';
import 'package:nissan_survey/src/widgets/SubmissionPopup.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'QuestionAnswerList.dart';

class ReviewScreen extends StatefulWidget {
  final Department department;
  final List<QuestionArea> questionArea;
  final String surveyId;
  final VoidCallback callback;

  ReviewScreen(
      this.department, this.questionArea, this.surveyId, this.callback);
  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/graphic-bg-inner.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Column(children: [
            AppHeaderView.addAppHeader(
                "assets/back-arrow.png",
                widget.department.selectedCity.selectedBranch.name,
                () => Navigator.of(context).pop()),
            SizedBox(height: 30),
            Expanded(
              child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView(
                    children: addQuestionAnswerList(),
                  )),
            ),
          ]),
        ));
  }

  List<Widget> addQuestionAnswerList() {
    List<Widget> quesAns = [];
    for (var index = 0; index < widget.questionArea.length + 1; index++) {
      if (widget.questionArea.length == index) {
        quesAns.add(addBottomActionButtonBar());
      } else {
        quesAns.add(
            QuestionAnswerList(false, index + 1, widget.questionArea[index]));
      }
    }
    return quesAns;
  }

  Widget addBottomActionButtonBar() {
    double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        addBottomButton(
            Row(children: [
              widget.department.parent == ParentInfo.mySurvey
                  ? Image.asset("assets/ARROW-L.png")
                  : Image.asset("assets/edit.png"),
              Text(
                widget.department.parent == ParentInfo.mySurvey
                    ? "BACK"
                    : "  EDIT",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              )
            ]),
            buttonWidth, () {
          widget.callback();
          Navigator.of(context).pop();
        }),
        addBottomButton(
            Row(children: [
              Text(
                widget.department.parent == ParentInfo.mySurvey
                    ? "SHARE  "
                    : "SUBMIT  ",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w400),
              ),
              widget.department.parent == ParentInfo.mySurvey
                  ? Image.asset("assets/share.png")
                  : Image.asset("assets/submit.png"),
            ]),
            buttonWidth, () {
          if (widget.department.parent == ParentInfo.mySurvey) {
            //Share
            showModalBottomSheet(
                context: context,
                builder: (BuildContext bc) {
                  return Container(
                    color: Colors.white,
                    child: new Wrap(
                      children: <Widget>[
                        new ListTile(
                            leading: new Icon(Icons.share,
                                color: AppColors.appThemeColor),
                            title: new Text('Share',
                                style: TextStyle(color: Colors.black)),
                            onTap: () {
                              Navigator.of(context).pop();
                              Share.share(
                                  ApiConstants.exportPDFUrl + widget.surveyId);
                            }),
                        new ListTile(
                          leading: new Icon(Icons.file_download,
                              color: AppColors.appThemeColor),
                          title: new Text('Download',
                              style: TextStyle(color: Colors.black)),
                          onTap: () async {
                            Navigator.of(context).pop();
                            ProgressBarUtil.showLoader(context);
                            NetworkApiUtil.downloadPDF(widget.surveyId)
                                .then((value) {
                              Navigator.of(context).pop();
                              value != null
                                  ? AppUtil.showToast(
                                      AppConstants.pdfDownloadSuccess, false)
                                  : AppUtil.showToast(
                                      AppConstants.pdfDownloadFailed, false);
                            });
                          },
                        ),
                        new ListTile(
                          leading: new Icon(Icons.close,
                              color: AppColors.appThemeColor),
                          title: new Text('Cancel',
                              style: TextStyle(color: Colors.black)),
                          onTap: () => Navigator.of(context).pop(),
                        ),
                      ],
                    ),
                  );
                });
          } else {
            //Submit
            showDialog<void>(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context) {
                return SubmissionPopup(widget.department, widget.questionArea);
              },
            );
          }
        })
      ]),
    );
  }

  Widget addBottomButton(
      Widget buttonTitle, double width, VoidCallback buttonAction) {
    return ButtonTheme(
      minWidth: width,
      height: 50.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () => buttonAction(),
        color: AppColors.appThemeColor,
        child: buttonTitle,
      ),
    );
  }
}
