import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'QuestionAnswerList.dart';

class QuestionnairePage extends StatefulWidget {
  final PageInfo info;
  final QuestionArea questionArea;
  final Function(PageAction) nextPressed;
  QuestionnairePage(this.info, this.questionArea, this.nextPressed);

  @override
  _QuestionnairePageState createState() => _QuestionnairePageState();
}

class _QuestionnairePageState extends State<QuestionnairePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
          child: Column(children: [
        AppHeaderView.addAppHeader(
            "assets/back-arrow.png",
            widget.info.department.selectedCity.selectedBranch.name,
            () => Navigator.of(context).pop()),
        Padding(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: addSurveyHeader()),
        QuestionAnswerList(true, widget.info.pageNum, widget.questionArea),
        addBottomActionButtonBar()
      ])),
    );
  }

  Widget addSurveyHeader() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(3.0),
          )),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: EdgeInsets.only(top: 22, bottom: 18, left: 20, right: 20),
          child: Text(
              "${widget.info.department.name} - ${widget.questionArea.type} - ${widget.questionArea.area} # ${widget.info.pageNum}/${widget.info.totalPageCount}",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w400)),
        ),
        Padding(
            padding: EdgeInsets.only(bottom: 22, left: 20, right: 20),
            child: LinearPercentIndicator(
              linearStrokeCap: LinearStrokeCap.roundAll,
              padding: EdgeInsets.all(0),
              animation: true,
              animationDuration: 1000,
              lineHeight: 5.0,
              percent: widget.info.pageNum / widget.info.totalPageCount,
              progressColor: AppColors.yellowProgressColor,
            )),
      ]),
    );
  }

  Widget addBottomActionButtonBar() {
    if (widget.info.totalPageCount == 1) {
      //Add Submit Button
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child: addBottomButton(
              Text("SUBMIT",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400)),
              MediaQuery.of(context).size.width - 40,
              () => widget.nextPressed(PageAction.submit)));
    } else if (widget.info.pageNum == 1) {
      //Add Next Button
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child: addBottomButton(
              Text("NEXT",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400)),
              MediaQuery.of(context).size.width - 40,
              () => widget.nextPressed(PageAction.next)));
    } else if (widget.info.pageNum == widget.info.totalPageCount) {
      //Add Back and Review Button
      double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            addBottomButton(
                Row(children: [
                  Image.asset("assets/ARROW-L.png"),
                  Text("BACK",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                ]),
                buttonWidth,
                () => widget.nextPressed(PageAction.back)),
            addBottomButton(
                Row(children: [
                  Text("REVIEW",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                  Image.asset("assets/ARROW-R.png"),
                ]),
                buttonWidth,
                () => widget.nextPressed(PageAction.review))
          ]));
    } else {
      //Add Back and Next Button
      double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            addBottomButton(
                Row(children: [
                  Image.asset("assets/ARROW-L.png"),
                  Text("BACK",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                ]),
                buttonWidth,
                () => widget.nextPressed(PageAction.back)),
            addBottomButton(
                Row(children: [
                  Text("NEXT",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                  Image.asset("assets/ARROW-R.png"),
                ]),
                buttonWidth,
                () => widget.nextPressed(PageAction.next))
          ]));
    }
  }

  Widget addBottomButton(
      Widget buttonTitle, double width, VoidCallback buttonAction) {
    return ButtonTheme(
      minWidth: width,
      height: 50.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          buttonAction();
        },
        color: AppColors.appThemeColor,
        child: buttonTitle,
      ),
    );
  }
}

class PageInfo {
  final Department department;
  final int pageNum;
  final int totalPageCount;
  PageInfo(this.department, this.pageNum, this.totalPageCount);
}

enum PageAction {
  next,
  back,
  review,
  submit,
}
