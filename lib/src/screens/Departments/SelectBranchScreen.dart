import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/screens/SurveyHistory/SurveyListScreen.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'SurveyQuestionnaireScreen.dart';

class SelectBranchScreen extends StatefulWidget {
  final String headerTitle;
  final Department department;
  SelectBranchScreen(this.headerTitle, this.department);

  @override
  _SelectBranchState createState() => _SelectBranchState();
}

class _SelectBranchState extends State<SelectBranchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader(
            "assets/back-arrow.png",
            widget.department.parent == ParentInfo.department
                ? widget.headerTitle + widget.department.selectedCity.name
                : widget.headerTitle + "",
            () => Navigator.of(context).pop()),
        Expanded(
            child: ListView.builder(
                //shrinkWrap: true,
                itemCount: widget.department.selectedCity.branches.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      widget.department.selectedCity.selectedBranch =
                          widget.department.selectedCity.branches[index];
                      if (widget.department.parent == ParentInfo.mySurvey) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                SurveyListScreen(widget.department)));
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                SurveyQuestionnaireScreen(widget.department)));
                      }
                    },
                    child: Column(children: [
                      addBranchRow(index),
                      Container(
                        height: 1,
                        color: AppColors.dividerLineColor,
                      )
                    ]),
                  );
                }))
      ]),
    ));
  }

  Widget addBranchRow(int index) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(left: 30),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(widget.department.selectedCity.branches[index].name,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
        ),
      ),
    );
  }
}
