import 'package:flutter/material.dart';
import 'package:nissan_survey/src/screens/Departments/DepartmentScreen.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';

class SurveyHistoryScreen extends StatefulWidget {
  @override
  _SurveyHistoryState createState() => _SurveyHistoryState();
}

class _SurveyHistoryState extends State<SurveyHistoryScreen> {
  List<String> surveyHistoryArray = ["My Surveys", "Admin Reviewed Surveys"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader("assets/MENU.png", "Surveys History", () {
          final ScaffoldState scaffoldState =
              context.findRootAncestorStateOfType();
          scaffoldState.openDrawer();
        }),
        Expanded(
            child: ListView.builder(
                itemCount: surveyHistoryArray.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      AppConstants.isReviewed = index.toString();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => DepartmentScreen(
                              ParentInfo.mySurvey, surveyHistoryArray[index])));
                    },
                    child: Column(children: [
                      addMySurveyRow(index),
                      Container(
                        height: 1,
                        color: AppColors.dividerLineColor,
                      )
                    ]),
                  );
                }))
      ]),
    ));
  }

  Widget addMySurveyRow(int index) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      color: Colors.white,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Padding(
            padding: EdgeInsets.only(left: 30),
            child: Text(surveyHistoryArray[index].toUpperCase(),
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500))),
        Padding(
            padding: EdgeInsets.only(right: 30),
            child: Image.asset("assets/arrow.png"))
      ]),
    );
  }
}
