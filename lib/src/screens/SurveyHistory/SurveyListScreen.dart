import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/MySurveyModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/Departments/ReviewScreen.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:intl/intl.dart';

class SurveyListScreen extends StatefulWidget {
  final Department department;
  SurveyListScreen(this.department);

  @override
  _SurveyListState createState() => _SurveyListState();
}

class _SurveyListState extends State<SurveyListScreen> {
  Future<MySurveyModel> response;

  @override
  void initState() {
    super.initState();
    response = ApiController.getSurveyApi(widget.department);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader("assets/back-arrow.png", "Select Date", () {
          Navigator.of(context).pop();
        }),
        FutureBuilder(
          future: response,
          builder: (context, projectSnap) {
            if (projectSnap.connectionState == ConnectionState.none &&
                projectSnap.hasData == null) {
              return Container();
            } else {
              if (projectSnap.hasData) {
                MySurveyModel model = projectSnap.data;
                if (model.success) {
                  return addExpandableYearContainer(model.years);
                } else {
                  return Container();
                }
              } else {
                return Padding(
                    padding: EdgeInsets.only(top: 100),
                    child: Center(
                      child: CircularProgressIndicator(
                          backgroundColor: AppColors.appThemeColor,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.black26)),
                    ));
              }
            }
          },
        )
      ]),
    ));
  }

  Widget addExpandableYearContainer(List<YearSurveyModel> years) {
    return Expanded(
        child: ListView.builder(
      itemCount: years.length,
      itemBuilder: (context, i) {
        return Container(
            color: Colors.white,
            child: Theme(
              data: ThemeData(accentColor: Colors.black54),
              child: ExpansionTile(
                  title: Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        years[i].year,
                        style: new TextStyle(
                            fontSize: 18.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500),
                      )),
                  children: addExpandableMonthContainer(
                    years[i].months,
                  )),
            ));
      },
    ));
  }

  List<Widget> addExpandableMonthContainer(List<MonthSurveyModel> months) {
    List<Widget> monthContent = [];
    for (MonthSurveyModel currentMonth in months) {
      monthContent.add(
        Container(
            color: AppColors.lightGrayBgColor,
            child: ExpansionTile(
              title: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Text(
                    (currentMonth.month + " (${currentMonth.surveys.length})"),
                    style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w400),
                  )),
              children: _buildExpandableContent(currentMonth.surveys),
            )),
      );
    }
    return monthContent;
  }

  _buildExpandableContent(List<Survey> surveys) {
    List<Widget> columnContent = [];
    for (Survey currentSurvey in surveys)
      columnContent.add(
        InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ReviewScreen(
                      widget.department, currentSurvey.area, currentSurvey.surveyId, () {})));
            },
            child: Container(
                height: 60,
                color: AppColors.darkGrayBgColor,
                child: ListTile(
                  title: RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text:
                            "           ${getStringDateFromString(currentSurvey.createDate, 'dd / MM / yyyy - EEE')} ",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w500)),
                    TextSpan(
                        text: currentSurvey.reviewed == "0"
                            ? ""
                            : "(${currentSurvey.reviewedBy})",
                        style: TextStyle(
                            color: AppColors.appThemeColor,
                            fontSize: 15,
                            fontWeight: FontWeight.w400))
                  ])),
                ))),
      );
    return columnContent;
  }

  String getStringDateFromString(DateTime date, String format) {
    var formatter = DateFormat(format);
    String formatted = formatter.format(date);
    return formatted;
  }
}
