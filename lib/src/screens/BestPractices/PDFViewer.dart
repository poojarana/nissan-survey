import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:nissan_survey/src/network/NetworkConstants.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';

class PDFViewerScreen extends StatefulWidget {
  final String pdfUrl;
  PDFViewerScreen(this.pdfUrl);

  @override
  _PDFViewerState createState() => _PDFViewerState();
}

class _PDFViewerState extends State<PDFViewerScreen> {
  bool _isLoading = true;
  PDFDocument document;

  @override
  void initState() {
    super.initState();
    loadDocument();
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(ApiConstants.pdfUrl + widget.pdfUrl);
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader("assets/back-arrow.png", "PDF Viewer",
            () => Navigator.of(context).pop()),
        SizedBox(height: 10),
        Center(
            child: _isLoading
                ? Center(
                    child: CircularProgressIndicator(
                        backgroundColor: AppColors.appThemeColor,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.black26)),
                  )
                : SizedBox(
                    height: MediaQuery.of(context).size.height - 130,
                    child: PDFViewer(document: document)))
      ]),
    ));
  }
}
