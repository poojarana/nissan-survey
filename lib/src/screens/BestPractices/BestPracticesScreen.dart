import 'package:flutter/material.dart';
import 'package:nissan_survey/src/screens/Departments/DepartmentScreen.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';

class BestPracticesScreen extends StatefulWidget {
  @override
  _BestPracticesState createState() => _BestPracticesState();
}

class _BestPracticesState extends State<BestPracticesScreen> {
  List<String> bestPractices = [
    "NISSAN \nGUIDELINES",
    "BEST PRACTICES \nBY MARKET",
    "ADD BEST \nPRACTICES",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/graphic-bg-inner.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Column(
            children: [
              AppHeaderView.addAppHeader("assets/MENU.png", "Best Practices",
                  () {
                final ScaffoldState scaffoldState =
                    context.findRootAncestorStateOfType();
                scaffoldState.openDrawer();
              }),
              SizedBox(height: 30),
              MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: Expanded(
                    child: ListView.builder(
                        itemCount: bestPractices.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              if (index == 0) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => DepartmentScreen(
                                        ParentInfo.viewGuidelines,
                                        "Nissan Guidelines")));
                              } else {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => DepartmentScreen(
                                        index == 1
                                            ? ParentInfo.viewBestPractice
                                            : ParentInfo.createBestPractice,
                                        "Select Department")));
                              }
                            },
                            child: Stack(children: [
                              addPracticesCard(index),
                              index == 1
                                  ? Positioned(
                                      top: 5,
                                      right: 20,
                                      child: Image.asset(
                                          "assets/selected-icon.png"))
                                  : Container(),
                            ]),
                          );
                        })),
              )
            ],
          )),
    );
  }

  Widget addPracticesCard(int index) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(15, index == 1 ? 20 : 15, 15, 40),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(index == 0
                  ? "assets/Red.png"
                  : (index == 1 ? "assets/DarkGrey.png" : "assets/Grey.png")),
              fit: BoxFit.fill,
            ),
          ),
          child: Padding(
              padding: EdgeInsets.fromLTRB(30, 20, 20, 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset(index == bestPractices.length - 1
                      ? "assets/add-practices.png"
                      : "assets/nissan-logo-small.png"),
                  SizedBox(height: 15),
                  Text(bestPractices[index],
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w400)),
                  SizedBox(height: 10),
                  Container(height: 2.0, width: 50, color: Colors.white),
                ],
              )),
        ));
  }
}
