import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/CategoryModel.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/widgets/PhotoCapturePopup.dart';
import 'package:nissan_survey/src/widgets/PracticeSubmissionPopup.dart';

class AddPracticesScreen extends StatefulWidget {
  final Department department;
  final Category category;
  AddPracticesScreen(this.department, this.category);

  @override
  _AddPracticesState createState() => _AddPracticesState();
}

class _AddPracticesState extends State<AddPracticesScreen> {
  List<ImageModel> images = [];
  TextEditingController commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/graphic-bg-inner.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          child: SingleChildScrollView(
              child: Column(
            children: [
              AppHeaderView.addAppHeader("assets/back-arrow.png",
                  widget.category.name, () => Navigator.of(context).pop()),
              SizedBox(height: 30),
              Padding(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        addImagesListView(),
                        Container(
                          height: images.length == 0 ? 200 : 140,
                          color: Color(0xFFE1E1E1),
                          child: InkWell(
                            onTap: (() {
                              FocusScope.of(context).requestFocus(FocusNode());
                              showDialog<void>(
                                barrierDismissible: true,
                                context: context,
                                builder: (BuildContext context) {
                                  return PhotoCapturePopup((image) {
                                    ImageModel model = ImageModel();
                                    model.localImage = image;
                                    model.imageName =
                                        DateTime.now().toIso8601String();
                                    images.add(model);
                                    setState(() {});
                                  });
                                },
                              );
                            }),
                            child: Center(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                  Image.asset("assets/add-image.png"),
                                  Padding(
                                      padding:
                                          EdgeInsets.only(top: 15, bottom: 20),
                                      child: Text("ADD PICTURES",
                                          style: TextStyle(
                                              color: AppColors.labelGrayColor,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w400)))
                                ])),
                          ),
                        ),
                        addCommentView(),
                        Padding(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            child: addBottomButton(
                                Text("DONE",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w400)),
                                MediaQuery.of(context).size.width - 40))
                      ])),
            ],
          ))),
    ));
  }

  Widget addImagesListView() {
    return images.length == 0
        ? Container()
        : Container(
            height: 150,
            padding: EdgeInsets.only(bottom: 30),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: images.length,
                itemBuilder: (context, index) {
                  return Stack(children: [
                    Padding(
                        padding: EdgeInsets.only(right: 8.0, top: 8),
                        child: Container(
                          height: 112,
                          width: 120,
                          child: ClipRRect(
                              child: FadeInImage(
                            placeholder: AssetImage("assets/Grey.png"),
                            fit: BoxFit.cover,
                            image: FileImage(images[index].localImage),
                          )),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.darkDividerLineColor,
                                width: 1),
                          ),
                        )),
                    Positioned(
                        right: 0,
                        child: InkWell(
                            onTap: () {
                              setState(() {
                                images.remove(images[index]);
                              });
                            },
                            child: Image.asset(
                              "assets/cross-icon.png",
                              width: 20,
                              height: 20,
                            )))
                  ]);
                }));
  }

  Widget addCommentView() {
    return Padding(
        padding: EdgeInsets.only(top: 35, bottom: 10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          TextField(
              controller: commentController,
              keyboardType: TextInputType.multiline,
              style: TextStyle(fontWeight: FontWeight.w400),
              minLines: 5, //Normal textInputField will be displayed
              maxLines: 5, //
              maxLength: 150, // when user presses enter it will adapt to it
              decoration: InputDecoration(
                labelText: "Enter Your Comments",
                labelStyle: TextStyle(
                    color: AppColors.labelGrayColor,
                    fontSize:
                        15.0, //I believe the size difference here is 6.0 to account padding
                    fontWeight: FontWeight.w400),
                alignLabelWithHint: true,
                contentPadding: const EdgeInsets.all(0.0),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.dividerLineColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.dividerLineColor),
                ),
              ))
        ]));
  }

  Widget addBottomButton(Widget buttonTitle, double width) {
    return ButtonTheme(
      minWidth: width,
      height: 50.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (images.length == 0) {
            AppUtil.showToast(AppConstants.addImage, true);
          } else if (commentController.text.trim().isEmpty) {
            AppUtil.showToast(AppConstants.enterComment, true);
          } else {
            ProgressBarUtil.showLoader(context);
            if (images.length > 0) {
              var futures = <Future>[];
              for (ImageModel img in images) {
                futures.add(AmazonS3Cognito.upload(
                    img.localImage.path,
                    "nissanapp/survey",
                    "ap-south-1:d0ef0317-aefd-4c78-abc8-8b7b1f1e0369",
                    img.imageName,
                    AwsRegion.AP_SOUTH_1,
                    AwsRegion.AP_SOUTH_1)
                    .then((value) {
                  value = value.replaceAll(
                      AppConstants.S3BucketPackageUrl, AppConstants.S3BucketUrl);
                  img.imageUrl = value;
                }));
              }
              Future.wait(futures).then((alUrls) {
                addBestPractice();
              });
            } else {
              AppUtil.showToast("Please add some images", true);
            }
          }
        },
        color: AppColors.appThemeColor,
        child: buttonTitle,
      ),
    );
  }

  void addBestPractice() {
    ApiController.addBestPracticesApi(
            widget.department, widget.category, commentController.text, images)
        .then((response) {
      Navigator.of(context).pop();
      if (response.success) {
        showDialog<void>(
          barrierDismissible: true,
          context: context,
          builder: (BuildContext context) {
            return PracticeSubmissionPopup();
          },
        );
      } else {
        AppUtil.showToast(response.message, true);
        Navigator.of(context).pop();
      }
    });
  }
}
