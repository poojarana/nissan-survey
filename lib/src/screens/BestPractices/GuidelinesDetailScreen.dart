import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/GuidelinesModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/BestPractices/PDFViewer.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';

class GuidelinesDetailScreen extends StatefulWidget {
  final Department department;
  GuidelinesDetailScreen(this.department);

  @override
  _GuidelinesDetailState createState() => _GuidelinesDetailState();
}

class _GuidelinesDetailState extends State<GuidelinesDetailScreen> {
  Future<dynamic> response;

  @override
  void initState() {
    super.initState();
    response = ApiController.getNissanGuidelinesApi(widget.department.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage('assets/graphic-bg-inner.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Column(
            children: [
              AppHeaderView.addAppHeader("assets/back-arrow.png",
                  widget.department.name, () => Navigator.of(context).pop()),
              SizedBox(height: 40),
              FutureBuilder(
                future: response,
                builder: (context, projectSnap) {
                  if (projectSnap.connectionState == ConnectionState.none &&
                      projectSnap.hasData == null) {
                    return Container();
                  } else {
                    if (projectSnap.hasData) {
                      GuidelinesModel model = projectSnap.data;
                      if (model.success) {
                        return Expanded(
                          child: MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              child: ListView.builder(
                                  itemCount: model.guidelines.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {},
                                      child: Stack(children: [
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                20, 5, 20, 40),
                                            child: (index + 1) % 2 == 0
                                                ? addWhiteCard(
                                                    model.guidelines[index])
                                                : addRedCard(
                                                    model.guidelines[index])),
                                        (index + 1) % 2 == 0
                                            ? addPDFIcon(
                                                AppColors.appThemeColor,
                                                Colors.white)
                                            : addPDFIcon(Colors.white,
                                                AppColors.appThemeColor)
                                      ]),
                                    );
                                  })),
                        );
                      } else {
                        return Container();
                      }
                    } else {
                      return Padding(
                          padding: EdgeInsets.only(top: 100),
                          child: Center(
                            child: CircularProgressIndicator(
                                backgroundColor: AppColors.appThemeColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.black26)),
                          ));
                    }
                  }
                },
              ),
            ],
          )),
    );
  }

  Widget addRedCard(Guidelines guideline) {
    return InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PDFViewerScreen(guideline.fileUrl)));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/Red.png"),
              fit: BoxFit.fill,
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black38,
                  blurRadius: 2.0,
                  offset: Offset(0.0, 0.90))
            ],
          ),
          child: Padding(
              padding: EdgeInsets.fromLTRB(25, 0, 25, 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset("assets/LOGO.png"),
                  SizedBox(height: 55),
                  Text(guideline.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w400)),
                  SizedBox(height: 10),
                  Container(height: 2.0, width: 50, color: Colors.white),
                ],
              )),
        ));
  }

  Widget addWhiteCard(Guidelines guideline) {
    return InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PDFViewerScreen(guideline.fileUrl)));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/White.png"),
              fit: BoxFit.fill,
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black38,
                  blurRadius: 2.0,
                  offset: Offset(0.0, 0.90))
            ],
          ),
          child: Padding(
              padding: EdgeInsets.fromLTRB(25, 0, 25, 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset("assets/LOGO.png"),
                  SizedBox(height: 55),
                  Text(guideline.title,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                  SizedBox(height: 10),
                  Container(
                      height: 2.0, width: 50, color: AppColors.appThemeColor),
                ],
              )),
        ));
  }

  Widget addPDFIcon(Color bgColor, Color txtColor) {
    return Positioned(
        top: 0,
        right: 12,
        child: Container(
          width: 44,
          height: 44,
          decoration: new BoxDecoration(
            color: bgColor,
            borderRadius: const BorderRadius.all(
              const Radius.circular(22.0),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 2.0,
                  offset: Offset(0.0, 0.75))
            ],
          ),
          child: Center(
              child: Text("PDF",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: txtColor))),
        ));
  }
}