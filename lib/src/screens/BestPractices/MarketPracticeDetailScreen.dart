import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/CategoryModel.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/MarketPracticeModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:nissan_survey/src/widgets/ImageViewer.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class MarketPracticesDetailScreen extends StatefulWidget {
  final Department department;
  final Category category;
  MarketPracticesDetailScreen(this.department, this.category);

  @override
  _MarketPracticesDetailState createState() => _MarketPracticesDetailState();
}

class _MarketPracticesDetailState extends State<MarketPracticesDetailScreen> {
  Future<MarketPracticeModel> response;

  PageController _controller;
  var currentPageValue = 0;

  @override
  void initState() {
    super.initState();
    response = ApiController.getMarketPracticesApi(
        widget.department.id, widget.category, "1");
    _controller = new PageController()..addListener(_listener);
  }

  _listener() {
    setState(() {
      currentPageValue = _controller.page.round();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage('assets/graphic-bg-inner.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ),
            child: Column(children: [
              AppHeaderView.addAppHeader("assets/back-arrow.png",
                  widget.category.name, () => Navigator.of(context).pop()),
              SizedBox(height: 20),
              FutureBuilder(
                  future: response,
                  builder: (context, projectSnap) {
                    if (projectSnap.connectionState == ConnectionState.none &&
                        projectSnap.hasData == null) {
                      return Container();
                    } else {
                      if (projectSnap.hasData) {
                        MarketPracticeModel model = projectSnap.data;
                        if (model.success) {
                          if (model.practices.length > 0) {
                            //count page numbers
                            int totalPages = 1;
                            if (model.totalCount > 10) {
                              int pages = model.totalCount ~/ 10;
                              int remainingItemCount = model.totalCount % 10;
                              totalPages = remainingItemCount == 0
                                  ? pages + 0
                                  : pages + 1;
                            }
                            return Expanded(
                              child: PageView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: totalPages,
                                  controller: _controller,
                                  itemBuilder: (context, index) {
                                    return PracticesList(
                                        index + 1,
                                        totalPages,
                                        widget.department,
                                        widget.category,
                                        index == 0 ? model : null, (action) {
                                      switch (action) {
                                        case ButtonAction.back:
                                          currentPageValue = index - 1;
                                          _controller.animateToPage(
                                              currentPageValue,
                                              duration:
                                                  Duration(milliseconds: 500),
                                              curve: Curves.ease);
                                          break;
                                        case ButtonAction.next:
                                          currentPageValue = index + 1;
                                          _controller.animateToPage(
                                              currentPageValue,
                                              duration:
                                                  Duration(milliseconds: 500),
                                              curve: Curves.ease);
                                          break;
                                        case ButtonAction.done:
                                          Navigator.of(context).pop();
                                          break;
                                      }
                                    });
                                  }),
                            );
                          } else {
                            return justShowHeader(true);
                          }
                        } else {
                          return justShowHeader(true);
                        }
                      } else {
                        return justShowHeader(false);
                      }
                    }
                  })
            ]),
          )),
    );
  }

  Widget justShowHeader(bool onlyHeader) {
    return Padding(
        padding: EdgeInsets.only(top: 100),
        child: onlyHeader
            ? Container()
            : Center(
                child: CircularProgressIndicator(
                    backgroundColor: AppColors.appThemeColor,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black26)),
              ));
  }
}

class PracticesList extends StatefulWidget {
  final int pageNum;
  final int totalPages;
  final Department department;
  final Category category;
  final MarketPracticeModel firstResponse;
  final Function(ButtonAction) callback;
  PracticesList(this.pageNum, this.totalPages, this.department, this.category,
      this.firstResponse, this.callback);

  @override
  _PracticesListState createState() => _PracticesListState();
}

class _PracticesListState extends State<PracticesList> {
  Future<MarketPracticeModel> response;

  @override
  void initState() {
    super.initState();
    if (widget.firstResponse == null) {
      response = ApiController.getMarketPracticesApi(
          widget.department.id, widget.category, widget.pageNum.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.firstResponse != null
        ? addPracticePage(widget.firstResponse.practices)
        : FutureBuilder(
            future: response,
            builder: (context, projectSnap) {
              if (projectSnap.connectionState == ConnectionState.none &&
                  projectSnap.hasData == null) {
                return Container();
              } else {
                if (projectSnap.hasData) {
                  MarketPracticeModel model = projectSnap.data;
                  if (model.success) {
                    if (model.practices.length > 0) {
                      return addPracticePage(model.practices);
                    } else {
                      return Container();
                    }
                  } else {
                    return Container();
                  }
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                        backgroundColor: AppColors.appThemeColor,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.black26)),
                  );
                }
              }
            });
  }

  Widget addPracticePage(List<MarketPractice> practices) {
    return SingleChildScrollView(
        child: Column(children: [
      Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 40),
          child: Container(
            width: MediaQuery.of(context).size.width - 20,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Color(0xFFD3D3D3),
                    blurRadius: 18.5,
                    offset: Offset(0.0, 8.0))
              ],
            ),
            child: Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: addPracticesList(practices)),
                )),
          )),
      addBottomActionButtonBar()
    ]));
  }

  List<Widget> addPracticesList(List<MarketPractice> practices) {
    List<Widget> quesAns = [];
    for (var index = 0; index < practices.length; index++) {
      quesAns
          .add(Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        index == 0
            ? Container(
                height: 60,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(practices[index].userName),
                      Text("${widget.pageNum}/${widget.totalPages}")
                    ]))
            : Container(),
        createUploadedImagesListView(practices[index]),
        enteredCommentView(practices[index])
      ]));
    }
    return quesAns;
  }

  Widget createUploadedImagesListView(MarketPractice practice) {
    return Container(
        height: 140,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: practice.images.length,
            itemBuilder: (context, index) {
              return Padding(
                  padding: EdgeInsets.only(right: 8.0),
                  child: InkWell(
                      onTap: () {
                        openImageViewPopup(practice.images[index]);
                      },
                      child: Container(
                        width: 150,
                        height: 130,
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: practice.images[index],
                          placeholder: (context, url) => Center(
                            child: CircularProgressIndicator(
                                backgroundColor: Colors.black26,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.black26)),
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: AppColors.darkDividerLineColor, width: 1),
                        ),
                      )));
            }));
  }

  void openImageViewPopup(String imageUrl) async {
    final cache = DefaultCacheManager();
    final file = await cache.getSingleFile(imageUrl);
    showDialog<void>(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return ImageViewerPopup(file);
      },
    );
  }

  Widget enteredCommentView(MarketPractice practice) {
    return practice.title == ""
        ? Container()
        : Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            SizedBox(height: 20),
            Text("Comments",
                style: TextStyle(
                    color: AppColors.labelGrayColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w400)),
            SizedBox(height: 10),
            Text(
              practice.title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 30),
                child: Container(
                  height: 1,
                  color: AppColors.darkDividerLineColor,
                )),
          ]);
  }

  Widget addBottomActionButtonBar() {
    if (widget.totalPages == 1 || widget.pageNum == widget.totalPages) {
      //Add Done Button
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child: addBottomButton(
              Text("DONE",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400)),
              MediaQuery.of(context).size.width - 40,
              () => widget.callback(ButtonAction.done)));
    } else if (widget.pageNum == 1) {
      //Add Next Button
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child: addBottomButton(
              Text("NEXT",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400)),
              MediaQuery.of(context).size.width - 40,
              () => widget.callback(ButtonAction.next)));
    } else {
      //Add Back and Next Button
      double buttonWidth = (MediaQuery.of(context).size.width - 60) / 2;
      return Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            addBottomButton(
                Row(children: [
                  Image.asset("assets/ARROW-L.png"),
                  Text("BACK",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                ]),
                buttonWidth,
                () => widget.callback(ButtonAction.back)),
            addBottomButton(
                Row(children: [
                  Text("NEXT",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400)),
                  Image.asset("assets/ARROW-R.png"),
                ]),
                buttonWidth,
                () => widget.callback(ButtonAction.next))
          ]));
    }
  }

  Widget addBottomButton(
      Widget buttonTitle, double width, VoidCallback buttonAction) {
    return ButtonTheme(
      minWidth: width,
      height: 50.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          buttonAction();
        },
        color: AppColors.appThemeColor,
        child: buttonTitle,
      ),
    );
  }
}

enum ButtonAction {
  next,
  back,
  done,
}
