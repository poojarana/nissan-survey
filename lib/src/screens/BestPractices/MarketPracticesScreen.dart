import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/CategoryModel.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';

import 'AddBestPracticesScreen.dart';
import 'MarketPracticeDetailScreen.dart';

class MarketPracticesScreen extends StatefulWidget {
  final Department department;
  MarketPracticesScreen(this.department);

  @override
  _MarketPracticesState createState() => _MarketPracticesState();
}

class _MarketPracticesState extends State<MarketPracticesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/graphic-bg-inner.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(children: [
        AppHeaderView.addAppHeader("assets/back-arrow.png", "Select Location",
            () {
          Navigator.of(context).pop();
        }),
        Expanded(
            child: widget.department.parent == ParentInfo.createBestPractice
                ? addBranchListView()
                : addWithoutBranchListView())
      ]),
    ));
  }

  Widget addBranchListView() {
    return ListView.builder(
      itemCount: widget.department.selectedCity.branches.length,
      itemBuilder: (context, i) {
        return new Container(
          color: Colors.white,
          child: Theme(
              data: ThemeData(accentColor: Colors.black54),
              child: ExpansionTile(
                title: new Text(
                  widget.department.selectedCity.branches[i].name,
                  style: new TextStyle(
                      fontSize: 15.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                children: addExpandablePracticeArea(
                  widget.department.selectedCity.branches[i],
                  widget.department.categories,
                ),
              )),
        );
      },
    );
  }

  Widget addWithoutBranchListView() {
    return ListView.builder(
      itemCount: widget.department.categories.length,
      itemBuilder: (context, i) {
        return new Container(
          color: Colors.white,
          child: Theme(
              data: ThemeData(accentColor: Colors.black54),
              child: ExpansionTile(
                title: new Text(
                  widget.department.categories[i].name,
                  style: new TextStyle(
                      fontSize: 15.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                children: _buildExpandableContent(
                    null, widget.department.categories[i].subAreas),
              )),
        );
      },
    );
  }

  List<Widget> addExpandablePracticeArea(
      Branch selectedBranch, List<CategoryModel> categories) {
    List<Widget> monthContent = [];
    for (CategoryModel categoryModel in categories) {
      monthContent.add(
        Container(
            color: AppColors.lightGrayBgColor,
            child: ExpansionTile(
              title: new Text(
                "        ${categoryModel.name}",
                style: new TextStyle(
                    fontSize: 15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400),
              ),
              children: _buildExpandableContent(
                  selectedBranch, categoryModel.subAreas),
            )),
      );
    }
    return monthContent;
  }

  _buildExpandableContent(Branch selectedBranch, List<Category> types) {
    List<Widget> columnContent = [];
    for (Category type in types)
      columnContent.add(
        InkWell(
            onTap: () {
              if (widget.department.parent == ParentInfo.createBestPractice) {
                widget.department.selectedCity.selectedBranch = selectedBranch;
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        AddPracticesScreen(widget.department, type)));
              } else {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => MarketPracticesDetailScreen(widget.department, type)));
              }
            },
            child: Container(
                height: 55,
                color: AppColors.darkGrayBgColor,
                child: ListTile(
                  title: Padding(
                      padding: EdgeInsets.only(left: 68),
                      child: Text(
                        ("${type.name}"),
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w400),
                      )),
                ))),
      );
    return columnContent;
  }
}
