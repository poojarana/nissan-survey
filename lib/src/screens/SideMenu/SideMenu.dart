import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/Login/LoginScreen.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';
import 'package:nissan_survey/src/screens/BestPractices/BestPracticesScreen.dart';
import 'package:nissan_survey/src/screens/Departments/DepartmentScreen.dart';
import 'package:nissan_survey/src/screens/SurveyHistory/SurveyHistoryScreen.dart';
import 'package:nissan_survey/src/screens/Profile/UserProfile.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class SideMenuPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SideMenuState();
  }
}

class _SideMenuState extends State<SideMenuPage> {
  int _selectedIndex = 1;
  String username = "";
  String imagePath;
  GlobalKey _drawerKey = GlobalKey();
  bool isBackPressedTwice = false;

  final _drawerItems = [
    DrawerChildItem("Departments", Image.asset('assets/department-icon.png')),
    DrawerChildItem(
        "Surveys History", Image.asset('assets/survey-history-icon.png')),
    DrawerChildItem(
        "Best Practices", Image.asset('assets/best-practices-icon.png')),
    DrawerChildItem("User Profile", Image.asset('assets/Profile-icon.png')),
    DrawerChildItem("Logout", Image.asset('assets/Logout-icon.png')),
  ];

  @override
  void initState() {
    super.initState();
    getUser();
  }

  void getUser() async {
    UserModel model = await SharedPrefs.getUser();
    username = model.fullName;
    if (model.profilePicture != null && model.profilePicture != "") {
      final directory = await getApplicationDocumentsDirectory();
      imagePath = '${directory.path}/profileImage.png';
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: false,
        drawer: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: AppColors.appThemeColor,
            ),
            child: Container(
                color: Colors.transparent,
                width: MediaQuery.of(context).size.width,
                child: Stack(children: [
                  SizedBox(
                      width: AppConstants.isLaunching
                          ? MediaQuery.of(context).size.width
                          : MediaQuery.of(context).size.width - 60,
                      child: Drawer(
                        key: _drawerKey,
                        child: ListView.builder(
                            itemCount: _drawerItems.length + 1,
                            itemBuilder: (BuildContext context, int index) {
                              return (index == 0
                                  ? createHeaderInfoItem()
                                  : createDrawerItem(index));
                            }),
                      )),
                  AppConstants.isLaunching
                      ? Container()
                      : Positioned(
                          top: 53,
                          right: 43,
                          child: InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: Image.asset("assets/cross-icon.png")))
                ]))),
        body: WillPopScope(
            child: _openPageForIndex(_selectedIndex), onWillPop: onWillPop));
  }

  Future<bool> onWillPop() async {
    final RenderBox box = _drawerKey.currentContext?.findRenderObject();
    if (box != null) {
      //is visible
      Navigator.of(context).pop();
    }

    if (isBackPressedTwice) {
      return true;
    } else {
      isBackPressedTwice = true;
      return false;
    }
  }

  // Create Layouts 
  Widget createHeaderInfoItem() {
    return Padding(
        padding: EdgeInsets.only(left: 27, top: 35),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Image.asset('assets/nissanTextLogo.png'),
          SizedBox(height: 60),
          Row(children: [
            CircleAvatar(
                radius: 30,
                backgroundImage: imagePath == null
                    ? AssetImage('assets/defaultUser.png')
                    : FileImage(File(imagePath))),
            SizedBox(width: 10),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                  text: "Welcome, ",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500)),
              TextSpan(
                  text: username,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w400))
            ])),
          ]),
          SizedBox(height: 40),
        ]));
  }

  Widget createDrawerItem(int index) {
    var item = _drawerItems[index - 1];
    return Padding(
        padding: EdgeInsets.only(left: 12),
        child: Container(
          height: 70,
          color: Colors.transparent,
          child: ListTileTheme(
            textColor: Colors.white,
            selectedColor: Colors.white,
            child: ListTile(
              leading: item.icon,
              title: Align(
                child: Text(item.title,
                    style: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w400)),
                alignment: Alignment(-1.1, -0.2),
              ),
              selected: index == _selectedIndex,
              onTap: () {
                AppConstants.isLaunching = false;
                index == 5 ? logoutAPI() : _onSelectItem(index);
              },
            ),
          ),
        ));
  }

  // Button Action 
  _openPageForIndex(int pos) {
    switch (pos) {
      case 1:
        return DepartmentScreen(ParentInfo.department, "Select Department");
      case 2:
        return SurveyHistoryScreen();
      case 3:
        return BestPracticesScreen();
      case 4:
        return UserProfileScreen();
      default:
        break;
    }
  }

  void _onSelectItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
    Navigator.of(context).pop();
  }

  void logoutAPI() {
    AppUtil.checkInternet().then((value) {
      if (value) {
        ProgressBarUtil.showLoader(context);
        ApiController.logoutApi().then((responseModel) {
          Navigator.of(context).pop();
          if (responseModel.success) {
            SharedPrefs.removeUser();
            SharedPrefs.setUserLoggedIn(false);
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => LoginScreen()),
                (Route<dynamic> route) => false);
          } else {
            AppUtil.showToast(responseModel.message, false);
          }
        });
      } else {
        AppUtil.showToast(AppConstants.noConnection, false);
      }
    });
  }
}

class DrawerChildItem {
  String title;
  Image icon;
  DrawerChildItem(this.title, this.icon);
}
