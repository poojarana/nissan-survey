class UserModel {
  String message;
  bool success;

  String token;
  String userId;
  String email;
  String fullName;
  String profilePicture;

  String address;
  String zipCode;
  String city;
  String state;

  String phoneNumber;
  String role;

  UserModel();

  factory UserModel.fromJson(Map<String, dynamic> json) {
    UserModel model = UserModel();
    model.message = json["message"];
    model.success = json["success"];

    dynamic data = json["data"];
    if (data != null) {
      model.token = json["token"];
      model.userId = data["_id"];
      model.email = data["email"];
      model.fullName = data["fullName"];
      model.profilePicture = data["profilePicture"];

      model.address = data["address"];
      model.zipCode = data["zipcode"];
      model.city = data["city"];
      model.state = data["state"];

      model.phoneNumber = data["phoneNumber"];
      model.role = data["roles"].contains("user") ? "user" : "admin";
    }
    return model;
  }

  Map<String, dynamic> toJson() => {
        "token": token,
        "data": {
          "_id": userId,
          "email": email,
          "fullName": fullName,
          "profilePicture": profilePicture,
          "address": address,
          "zipcode": zipCode,
          "city": city,
          "state": state,
          "phoneNumber": phoneNumber,
          "roles": [role]
        }
      };
}

class ResponseModel {
  String message;
  bool success;

  ResponseModel();

  factory ResponseModel.fromJson(Map<String, dynamic> json) {
    ResponseModel model = ResponseModel();
    model.message = json["message"];
    model.success = json["success"];
    return model;
  }
}

class RememberModel {
  bool isRemember;
  String email;
  String password;

  RememberModel(this.isRemember, this.email, this.password);
}
