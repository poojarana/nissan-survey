class CategoryModel {
  String name;
  List<Category> subAreas;

  CategoryModel();

  factory CategoryModel.fromJson(String name, List<dynamic> json) {
    CategoryModel model = CategoryModel();
    model.name = name;
    model.subAreas = List<Category>.from(json.map((x) => Category.fromJson(x)));
    return model;
  }
}

class Category {
  String id;
  String type;
  String name;
  Category();

  factory Category.fromJson(Map<String, dynamic> json) {
    Category model = Category();
    model.id = json["_id"];
    model.type = json["type"];
    model.name = json["name"];
    return model;
  }
}
