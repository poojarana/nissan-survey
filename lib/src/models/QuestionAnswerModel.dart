import 'package:flutter/material.dart';
import 'dart:io';

class QuestionAnswerModel {
  String message;
  bool success;
  List<QuestionArea> questionArea;

  QuestionAnswerModel();

  factory QuestionAnswerModel.fromJson(Map<String, dynamic> json) {
    QuestionAnswerModel model = QuestionAnswerModel();
    model.message = json["message"];
    model.success = json["success"];
    if (json["data"] != null) {
      model.questionArea = List<QuestionArea>.from(
          json["data"].map((x) => QuestionArea.fromJson(x)));
    }
    return model;
  }
}

class QuestionArea {
  String id;
  String area;
  String type;
  List<ImageModel> images = [];
  List<Question> questions;
  String addedComment;
  TextEditingController comment = TextEditingController();

  QuestionArea();

  factory QuestionArea.fromJson(Map<String, dynamic> json) {
    QuestionArea model = QuestionArea();
    model.id = json["_id"];
    model.area = json["area"];
    model.type = json["type"];
    model.addedComment = json["comment"];

    if (json["images"] != null) {
      model.images = List<ImageModel>.from(
          json["images"].map((x) => ImageModel.fromJson(x)));
    }

    if (json["questions"] != null) {
      model.questions = List<Question>.from(
          json["questions"].map((x) => Question.fromJson(x)));
    }
    return model;
  }
}

class ImageModel {
  String imageUrl;
  File localImage;
  String imageName;

  ImageModel({
    this.imageUrl,
  });

  factory ImageModel.fromJson(String imageName) =>
      ImageModel(imageUrl: imageName);
}

class Question {
  int selection = 0;
  String name;

  Question();

  factory Question.fromJson(Map<String, dynamic> json) {
    Question model = Question();
    model.selection = json["selected"] == null ? 0 : json["selected"] ? 1 : 2;
    model.name = json["name"];
    return model;
  }
}
