
class MarketPracticeModel {
  bool success;
  int totalCount;
  List<MarketPractice> practices;

  MarketPracticeModel();

  factory MarketPracticeModel.fromJson(Map<String, dynamic> json) {
    MarketPracticeModel model = MarketPracticeModel();
    model.success = json["success"];
    model.totalCount = json["counts"];
    if (json["data"] != null) {
      model.practices = List<MarketPractice>.from(
          json["data"].map((x) => MarketPractice.fromJson(x)));
    }
    return model;
  }
}

class MarketPractice {
  String id;
  String title;
  String userName;
  List<String> images = [];

  MarketPractice();

  factory MarketPractice.fromJson(Map<String, dynamic> json) {
    MarketPractice model = MarketPractice();
    model.id = json["_id"];
    model.title = json["title"];
    if (json["user"] != null) {
      dynamic userData = json["user"];
      model.userName = userData["fullName"];
    }

    if (json["images"] != null) {
      model.images = List<String>.from(
          json["images"].map((x) => x));
    }
    return model;
  }
}