import 'package:nissan_survey/src/utils/AppConstants.dart';

import 'QuestionAnswerModel.dart';

class MySurveyModel {
  String message;
  bool success;
  List<YearSurveyModel> years;

  MySurveyModel();

  factory MySurveyModel.fromJson(Map<String, dynamic> json) {
    MySurveyModel model = MySurveyModel();
    model.message = json["message"];
    model.success = json["success"];

    if (json["data"] != null) {
      List<Survey> surveys =
          List<Survey>.from(json["data"].map((x) => Survey.fromJson(x)));
      List<Survey> selectedSurveys = surveys
          .where((element) => element.reviewed == AppConstants.isReviewed)
          .toList();
      model.years = filterWithYear(selectedSurveys);
    }
    return model;
  }
}

List<YearSurveyModel> filterWithYear(List<Survey> surveys) {
  List<YearSurveyModel> yearModels = [];
  for (Survey currentSurvey in surveys) {
    String year = currentSurvey.createDate.year.toString();
    var yearFound =
        yearModels.where((element) => element.year == year.toString()).toList();

    if (yearFound.length > 0) {
      //Year found then update year
      YearSurveyModel yearModel = yearFound[0];
      var monthFound = yearModel.months
          .where((element) =>
              element.month == getMonthString(currentSurvey.createDate.month))
          .toList();
      if (monthFound.length > 0) {
        //Month found then update month
        MonthSurveyModel monthModel = monthFound[0];
        monthModel.month = getMonthString(currentSurvey.createDate.month);
        monthModel.surveys.add(currentSurvey);
        monthModel.surveys.sort((a, b) => b.createDate.compareTo(a.createDate));
//        monthModel.surveys = monthModel.surveys.reversed.toList();
      } else {
        //Month not found then add month
        MonthSurveyModel monthModel = MonthSurveyModel();
        monthModel.month = getMonthString(currentSurvey.createDate.month);
        monthModel.surveys = [currentSurvey];
        yearModel.months.add(monthModel);
      }
    } else {
      //Year not found then add year
      YearSurveyModel yearModel = YearSurveyModel();
      yearModel.year = currentSurvey.createDate.year.toString();

      MonthSurveyModel monthModel = MonthSurveyModel();
      monthModel.month = getMonthString(currentSurvey.createDate.month);
      monthModel.surveys = [currentSurvey];
      yearModel.months = [monthModel];
      yearModels.add(yearModel);
    }
  }

//  yearModels.forEach((year) {
//    year.months.forEach((sur) {
//      sur.surveys.sort((a, b) => b.createDate.compareTo(a.createDate));
//    });
//  });

  return yearModels;
}

String getMonthString(int value) {
  String month = "";
  switch (value) {
    case 1:
      month = "January";
      break;
    case 2:
      month = "February";
      break;
    case 3:
      month = "March";
      break;
    case 4:
      month = "April";
      break;
    case 5:
      month = "May";
      break;
    case 6:
      month = "June";
      break;
    case 7:
      month = "July";
      break;
    case 8:
      month = "August";
      break;
    case 9:
      month = "September";
      break;
    case 10:
      month = "October";
      break;
    case 11:
      month = "November";
      break;
    case 12:
      month = "December";
      break;
  }
  return month;
}

class Survey {
  String surveyId;
  String departmentId;
  String branchName;
  String reviewed;
  String reviewedBy;
  DateTime createDate;

  List<QuestionArea> area;

  Survey();

  factory Survey.fromJson(Map<String, dynamic> json) {
    Survey model = Survey();

    model.surveyId = json["_id"];
    model.departmentId = json["department"];
    model.branchName = json["branch"];
    model.reviewed = json["reviewed"];
    model.reviewedBy = json["reviewedBy"];
    model.createDate = DateTime.parse(json["created"]);

    if (json["options"] != null) {
      model.area = List<QuestionArea>.from(
          json["options"].map((x) => QuestionArea.fromJson(x)));
    }
    return model;
  }
}

class YearSurveyModel {
  String year;
  List<MonthSurveyModel> months;
}

class MonthSurveyModel {
  String month;
  List<Survey> surveys;
}
