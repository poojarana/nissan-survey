class GuidelinesModel {
  bool success;
  String message;
  List<Guidelines> guidelines;

  GuidelinesModel();

  factory GuidelinesModel.fromJson(Map<String, dynamic> json) {
    GuidelinesModel model = GuidelinesModel();
    model.message = json["message"];
    model.success = json["success"];
    if (json["data"] != null) {
      model.guidelines = List<Guidelines>.from(
          json["data"].map((x) => Guidelines.fromJson(x)));
    }
    return model;
  }
}

class Guidelines {
  String id;
  String title;
  String fileUrl;

  Guidelines();

  factory Guidelines.fromJson(Map<String, dynamic> json) {
    Guidelines model = Guidelines();
    model.id = json["_id"];
    model.title = json["title"];
    model.fileUrl = json["file"];
    return model;
  }
}