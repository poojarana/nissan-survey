import 'package:nissan_survey/src/models/CategoryModel.dart';
import 'package:nissan_survey/src/widgets/AppHeaderView.dart';
import "package:collection/collection.dart";

class DepartmentModel {
  String message;
  bool success;
  List<Department> departments;

  DepartmentModel();

  factory DepartmentModel.fromJson(Map<String, dynamic> json) {
    DepartmentModel model = DepartmentModel();
    model.message = json["message"];
    model.success = json["success"];
    if (json["data"] != null) {
      model.departments = List<Department>.from(
          json["data"].map((x) => Department.fromJson(x)));
    }
    return model;
  }
}

class Department {
  String id;
  String name;
  CityModel selectedCity;
  ParentInfo parent;
  List<CityModel> cities;
  List<CategoryModel> categories = [];

  Department();

  factory Department.fromJson(Map<String, dynamic> json) {
    Department model = Department();
    model.id = json["_id"];
    model.name = json["name"];
    if (json["branches"] != null) {
      model.cities = List<CityModel>.from(
          json["branches"].map((x) => CityModel.fromJson(x)));
    }

    if (json["category"] != null) {
      var newMap = groupBy(json["category"], (obj) => obj['type']);
      newMap.forEach(
          (k, v) => model.categories.add(CategoryModel.fromJson(k, v)));
    }
    return model;
  }
}

class CityModel {
  String id;
  String name;
  Branch selectedBranch;
  List<Branch> branches;

  CityModel();

  factory CityModel.fromJson(Map<String, dynamic> json) {
    CityModel model = CityModel();
    model.id = json["_id"];
    model.name = json["city"];
    if (json["branches"] != null) {
      model.branches =
          List<Branch>.from(json["branches"].map((x) => Branch.fromJson(x)));
    }
    return model;
  }
}

class Branch {
  String branchId;
  String name;
  List<PracticeArea> marketPractices = [
    PracticeArea.fromJson("Exterior"),
    PracticeArea.fromJson("Interior"),
    PracticeArea.fromJson("Business & Process")
  ];

  Branch();

  factory Branch.fromJson(Map<String, dynamic> json) {
    Branch model = Branch();
    model.branchId = json["_id"];
    model.name = json["name"];
    return model;
  }
}

class PracticeArea {
  String name;
  List<String> types;

  PracticeArea();

  factory PracticeArea.fromJson(String practiceName) {
    PracticeArea model = PracticeArea();
    model.name = practiceName;
    model.types = ["Exterior Signages", "Customer Parking Space"];
    return model;
  }
}
