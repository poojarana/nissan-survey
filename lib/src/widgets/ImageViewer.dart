import 'dart:io';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageViewerPopup extends StatefulWidget {
  final File imageUrl;
  ImageViewerPopup(this.imageUrl);

  @override
  State<StatefulWidget> createState() {
    return ImageViewerState();
  }
}

class ImageViewerState extends State<ImageViewerPopup>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(20),
        child: Stack(children: [
          Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Image.asset("assets/cross-icon.png"))),
          Container(
              padding: EdgeInsets.only(top: 35),
              height: MediaQuery.of(context).size.height - 180,
              child: PhotoView(
                imageProvider: FileImage(widget.imageUrl),
              ))
        ]));
  }
}
