import 'dart:io';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:flutter/material.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:nissan_survey/src/network/ApiController.dart';
import 'package:nissan_survey/src/screens/SideMenu/SideMenu.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/AppUtil.dart';
import 'package:nissan_survey/src/utils/ProgressBarUtil.dart';
import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';

class SubmissionPopup extends StatefulWidget {
  final Department department;
  final List<QuestionArea> questionArea;
  SubmissionPopup(this.department, this.questionArea);

  @override
  State<StatefulWidget> createState() {
    return SubmissionPopupState();
  }
}

class SubmissionPopupState extends State<SubmissionPopup>
    with SingleTickerProviderStateMixin {
  String location = "";

  void initState() {
    super.initState();
    location =
    "${widget.department.selectedCity.selectedBranch.name.toUpperCase()} - ${widget.department.name.toUpperCase()}";
    getUser();
  }

  void getUser() async {
    UserModel model = await SharedPrefs.getUser();
    location =
    "${model.city.toUpperCase()} - ${widget.department.selectedCity.selectedBranch.name.toUpperCase()} - ${widget.department.name.toUpperCase()}";
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: Stack(children: [
            Container(
                height: 450,
                child: Padding(
                  padding: EdgeInsets.only(top: 45, left: 15, right: 15),
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: AppColors.appThemeColor,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 55, bottom: 10),
                              child: Text("CONGRATULATIONS",
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontFamily:
                                          Platform.isIOS ? "Roboto" : "DIN",
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      decoration: TextDecoration.none))),
                          Container(
                              height: 1.5, width: 100, color: Colors.white),
                          Padding(
                              padding: EdgeInsets.only(top: 10, bottom: 20),
                              child: Text(
                                  "The Field Force \nChecklist Has Been \nCompleted Successfully",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: Platform.isIOS ? 28 : 30,
                                      fontFamily:
                                          Platform.isIOS ? "Roboto" : "DIN",
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.none))),
                          Padding(
                              padding: EdgeInsets.only(bottom: 20),
                              child:
                                  Container(height: 1, color: Colors.white24)),
                          Padding(
                              padding: EdgeInsets.only(left: 6, right: 6),
                              child: Center(
                                  child: Text("LOCATION: $location",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily:
                                              Platform.isIOS ? "Roboto" : "DIN",
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          decoration: TextDecoration.none)))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(6, 10, 6, 30),
                              child: Text(
                                  "Please click the button below to submit",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily:
                                          Platform.isIOS ? "Roboto" : "DIN",
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.none))),
                          addOkayButton("OKAY")
                        ],
                      )),
                )),
            Positioned.fill(
              child: Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset("assets/congratulations.png")),
            )
          ]),
        ));
  }

  Widget addOkayButton(String title) {
    return ButtonTheme(
      minWidth: (MediaQuery.of(context).size.width - 60) / 2,
      height: 45.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () {
          ProgressBarUtil.showLoader(context);
          var allAreaImages =
              widget.questionArea.map((area) => area.images).toList();
          List<ImageModel> flattened =
              allAreaImages.expand((images) => images).toList();

          if (flattened.length > 0) {
            var futures = <Future>[];
            for (ImageModel img in flattened) {
              futures.add(AmazonS3Cognito.upload(
                      img.localImage.path,
                      "nissansurveyapp/survey",
                      "us-east-1:c691b7ce-492a-4761-bf25-e321db9d1019",
                      img.imageName,
                      AwsRegion.US_EAST_1,
                      AwsRegion.US_EAST_1)
                  .then((value) {
                value = value.replaceAll(
                    AppConstants.S3BucketPackageUrl, AppConstants.S3BucketUrl);
                img.imageUrl = value;
              }));
            }
            Future.wait(futures).then((alUrls) {
              uploadSurvey();
            });
          } else {
            uploadSurvey();
          }
        },
        color: Colors.white,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 16,
              fontFamily: Platform.isIOS ? "Roboto" : "DIN",
              color: AppColors.appThemeColor,
              fontWeight: FontWeight.w400),
        ),
      ),
    );
  }

  void uploadSurvey() {
    ApiController.submitSurveyApi(widget.department, widget.questionArea)
        .then((response) {
      Navigator.of(context).pop();
      if (response.success) {
        AppConstants.isLaunching = true;
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => SideMenuPage()),
            (Route<dynamic> route) => false);
      } else {
        AppUtil.showToast(response.message, true);
        Navigator.of(context).pop();
      }
    });
  }
}
