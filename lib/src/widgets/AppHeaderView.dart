import 'package:flutter/material.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';

class AppHeaderView {
  static Widget addAppHeader(String imageName, String title, VoidCallback callback) {
    return Padding(
      padding: EdgeInsets.only(left: 30, right: 10, top: 70, bottom: 10),
      child: Row(children: [
        InkWell(
            onTap: () => callback(),
            child: Image.asset(imageName)),
        SizedBox(width: 15),
        Flexible(child: Text(
          title,
          style: TextStyle(
              color: AppColors.appThemeColor,
              fontSize: 22,
              fontWeight: FontWeight.w500),
        ))
      ]),
    );
  }
}

enum ParentInfo {
  department,
  mySurvey,
  viewGuidelines,
  viewBestPractice,
  createBestPractice,
}