import 'dart:io';
import 'package:flutter/material.dart';
import 'package:nissan_survey/src/screens/SideMenu/SideMenu.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';

class PracticeSubmissionPopup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PracticeSubmissionState();
  }
}

class PracticeSubmissionState extends State<PracticeSubmissionPopup>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(children: [
        Container(
            height: 350,
            child: Padding(
              padding: EdgeInsets.only(top: 45, left: 15, right: 15),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: AppColors.appThemeColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 55, bottom: 10),
                          child: Text("CONGRATULATIONS",
                              style: TextStyle(
                                  fontSize: 17,
                                  fontFamily: Platform.isIOS ? "Roboto" : "DIN",
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  decoration: TextDecoration.none))),
                      Container(height: 1.5, width: 100, color: Colors.white),
                      Padding(
                          padding: EdgeInsets.only(top: 20, bottom: 25),
                          child: Text("Submitted \nSuccessfully",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: Platform.isIOS ? 28 : 30,
                                  fontFamily: Platform.isIOS ? "Roboto" : "DIN",
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  decoration: TextDecoration.none))),
                      Padding(
                          padding: EdgeInsets.only(bottom: 35),
                          child: Container(height: 1, color: Colors.white24)),
                      addOkayButton("OKAY")
                    ],
                  )),
            )),
        Positioned.fill(
          child: Align(
              alignment: Alignment.topCenter,
              child: Image.asset("assets/congratulations.png")),
        )
      ]),
    );
  }

  Widget addOkayButton(String title) {
    return ButtonTheme(
      minWidth: (MediaQuery.of(context).size.width - 60) / 2,
      height: 45.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: () {
          AppConstants.isLaunching = true;
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => SideMenuPage()),
              (Route<dynamic> route) => false);
        },
        color: Colors.white,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 16,
              fontFamily: Platform.isIOS ? "Roboto" : "DIN",
              color: AppColors.appThemeColor,
              fontWeight: FontWeight.w400),
        ),
      ),
    );
  }
}
