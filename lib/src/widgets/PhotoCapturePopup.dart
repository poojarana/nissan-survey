import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PhotoCapturePopup extends StatefulWidget {
  final Function(File) handler;
  PhotoCapturePopup(this.handler);

  @override
  State<StatefulWidget> createState() {
    return PhotoCapturePopupState();
  }
}

class PhotoCapturePopupState extends State<PhotoCapturePopup>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          height: 190,
          child: Container(
            width: MediaQuery.of(context).size.width - 40,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: EdgeInsets.only(
                        left: 30, right: 30, top: 30, bottom: 25),
                    child: Text("Add Photo!",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: Platform.isIOS ? "Roboto" : "DIN",
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none))),
                FlatButton(
                    padding: EdgeInsets.only(left: 30),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () async {
                      Navigator.of(context).pop();
                      File compressedImage = await ImagePicker.pickImage(
                          source: ImageSource.camera, imageQuality: 60);
                      if (compressedImage != null) {
                        widget.handler(compressedImage);
                      }
                    },
                    child: Row(children: [
                      Image.asset("assets/smallCamera.png"),
                      SizedBox(width: 15),
                      Text("Take Photo",
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20,
                              fontFamily: Platform.isIOS ? "Roboto" : "DIN",
                              fontWeight: FontWeight.w400)),
                    ])),
                FlatButton(
                    padding: EdgeInsets.only(left: 30),
                    onPressed: () async {
                      Navigator.of(context).pop();
                      File compressedImage = await ImagePicker.pickImage(
                          source: ImageSource.gallery, imageQuality: 60);
                      if (compressedImage != null) {
                        widget.handler(compressedImage);
                      }
                    },
                    child: Row(children: [
                      Image.asset("assets/gallery-icon.png"),
                      SizedBox(width: 15),
                      Text("Choose from Gallery",
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20,
                              fontFamily: Platform.isIOS ? "Roboto" : "DIN",
                              fontWeight: FontWeight.w400)),
                    ])),
                SizedBox(height: 5)
              ],
            ),
          )),
    );
  }
}
