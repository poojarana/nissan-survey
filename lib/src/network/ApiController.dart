import 'package:nissan_survey/src/models/CategoryModel.dart';
import 'package:nissan_survey/src/models/DepartmentModel.dart';
import 'package:nissan_survey/src/models/GuidelinesModel.dart';
import 'package:nissan_survey/src/models/MarketPracticeModel.dart';
import 'package:nissan_survey/src/models/MySurveyModel.dart';
import 'package:nissan_survey/src/models/QuestionAnswerModel.dart';
import 'package:nissan_survey/src/models/UserModel.dart';
import 'package:nissan_survey/src/network/NetworkApiCallUtil.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'NetworkConstants.dart';
import 'dart:io';
import 'dart:io' as Io;
import 'dart:async';
// import 'package:image/image.dart';

class ApiController {
  static Future<UserModel> loginApi(String email, String password) async {
    var url = ApiConstants.baseUrl + ApiConstants.login;
    Map<String, String> param = <String, String>{
      'email': email,
      'password': password,
      'deviceType': Platform.isIOS ? "IOS" : "Android",
    };

    return await NetworkApiUtil.postRequest(url, param, null)
        .then((dynamic res) async {
      UserModel model = UserModel.fromJson(res);
      if (model.success) {
        if (model.profilePicture != null && model.profilePicture != "") {
          await SaveFile().saveImage(model.profilePicture);
        }
      }
      return model;
    });
  }

  static Future<ResponseModel> forgotPasswordApi(String email) async {
    var url = ApiConstants.baseUrl + ApiConstants.forgotPassword;
    Map<String, String> param = <String, String>{
      'email': email,
    };
    return await NetworkApiUtil.postRequest(url, param, null)
        .then((dynamic res) {
      ResponseModel model = ResponseModel.fromJson(res);
      return model;
    });
  }

  static Future<ResponseModel> changePasswordApi(
      String oldP, String newP) async {
    var url = ApiConstants.baseUrl + ApiConstants.changePassword;
    UserModel model = await SharedPrefs.getUser();
    Map<String, String> param = <String, String>{
      'oldpassord': oldP,
      'newpassword': newP
    };
    return await NetworkApiUtil.postRequest(url, param, model.token)
        .then((dynamic res) {
      ResponseModel model = ResponseModel.fromJson(res);
      return model;
    });
  }

  static Future<ResponseModel> logoutApi() async {
    var url = ApiConstants.baseUrl + ApiConstants.logout;
    UserModel model = await SharedPrefs.getUser();
    return await NetworkApiUtil.putRequest(url, model.token)
        .then((dynamic res) {
      ResponseModel model = ResponseModel.fromJson(res);
      return model;
    });
  }

  static Future<DepartmentModel> getDepartmentsApi(String pageNumber) async {
    //    var url = ApiConstants.baseUrl + ApiConstants.getDepartments + pageNumber;
    var url = ApiConstants.baseUrl + ApiConstants.getDepartments;
    UserModel model = await SharedPrefs.getUser();
    return await NetworkApiUtil.getRequest(url, model.token)
        .then((dynamic res) {
      DepartmentModel model = DepartmentModel.fromJson(res);
      return model;
    });
  }

  static Future<QuestionAnswerModel> getDepartmentInfoApi(String option) async {
    var url = ApiConstants.baseUrl + ApiConstants.getDepartmentInfo + option;
    UserModel model = await SharedPrefs.getUser();
    return await NetworkApiUtil.getRequest(url, model.token)
        .then((dynamic res) {
      QuestionAnswerModel model = QuestionAnswerModel.fromJson(res);
      return model;
    });
  }

  static Future<MySurveyModel> getSurveyApi(Department department) async {
    var url = ApiConstants.baseUrl +
        ApiConstants.getSurvey +
        "?department=${department.id}&branch=${department.selectedCity.selectedBranch.branchId}";
    UserModel model = await SharedPrefs.getUser();
    return await NetworkApiUtil.getRequest(url, model.token)
        .then((dynamic res) {
      MySurveyModel model = MySurveyModel.fromJson(res);
      return model;
    });
  }

  static Future<QuestionAnswerModel> submitSurveyApi(
      Department department, List<QuestionArea> questionArea) async {
    var url = ApiConstants.baseUrl + ApiConstants.getSurvey;
    UserModel model = await SharedPrefs.getUser();

    Map param = {
      'department': department.id,
      'branch': department.selectedCity.selectedBranch.branchId,
      'options': getQuestionAreas(questionArea),
    };
    return await NetworkApiUtil.postRequest(url, param, model.token)
        .then((dynamic res) {
      QuestionAnswerModel model = QuestionAnswerModel.fromJson(res);
      return model;
    });
  }

  static List getQuestionAreas(List<QuestionArea> areas) {
    List<String> imageName = [];
    List options = [];
    for (var i = 0; i < areas.length; i++) {
      QuestionArea area = areas[i];
      imageName = area.images.map((e) => e.imageUrl).toList();
      options.add({
        "area": area.area,
        "images": imageName,
        "questions": getQuestionListWithAnswers(area.questions),
        "comment": area.comment.text
      });
    }
    return options;
  }

  static List getQuestionListWithAnswers(List<Question> questions) {
    List questionsArr = [];
    for (var i = 0; i < questions.length; i++) {
      questionsArr.add({
        "name": questions[i].name,
        "selected": questions[i].selection == 1 ? true : false,
      });
    }
    return questionsArr;
  }

  static Future<GuidelinesModel> getNissanGuidelinesApi(
      String departmentId) async {
    var url = ApiConstants.baseUrl + ApiConstants.guidelines;
    UserModel model = await SharedPrefs.getUser();
    Map param = {
      'department': departmentId,
    };
    return await NetworkApiUtil.postRequest(url, param, model.token)
        .then((dynamic res) {
      GuidelinesModel model = GuidelinesModel.fromJson(res);
      return model;
    });
  }

  static Future<MarketPracticeModel> getMarketPracticesApi(String departmentId,
      Category category, String page) async {
    var url = ApiConstants.baseUrl + ApiConstants.getMarketPractices + page;

    UserModel model = await SharedPrefs.getUser();
    Map param = {
      'department': departmentId,
      'area': category.id,
      'type': category.type,
    };
    return await NetworkApiUtil.postRequest(url, param, model.token)
        .then((dynamic res) {
      MarketPracticeModel model = MarketPracticeModel.fromJson(res);
      return model;
    });
  }

  static Future<ResponseModel> addBestPracticesApi(Department department,
      Category category, String title, List<ImageModel> images) async {
    var url = ApiConstants.baseUrl + ApiConstants.addMarketPractices;
    UserModel model = await SharedPrefs.getUser();
    List<String> imageNames = images.map((e) => e.imageUrl).toList();

    Map param = {
      'department': department.id,
      'branch': department.selectedCity.selectedBranch.branchId,
      'area': category.id,
      'type': category.type,
      'title': title,
      'comment': "",
      'images': imageNames,
    };
    return await NetworkApiUtil.postRequest(url, param, model.token)
        .then((dynamic res) {
      ResponseModel model = ResponseModel.fromJson(res);
      return model;
    });
  }
}

class SaveFile {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<FileInfo> getImageFromNetwork(String url) async {
    FileInfo info = await DefaultCacheManager().downloadFile(url);
    return info;
  }

  Future<Io.File> saveImage(String url) async {
    FileInfo info = await getImageFromNetwork(url);
    //retrieve local path for device
    var path = await _localPath;
    // Save the thumbnail as a PNG.
    return new Io.File('$path/profileImage.png')
      ..writeAsBytesSync(info.file.readAsBytesSync());
  }
}
