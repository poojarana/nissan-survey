class ApiConstants {
//  static String baseUrl = 'http://18.219.25.133:3001/api/';

  static String baseUrl = 'http://15.185.120.84/api/';
  static String pdfUrl = 'http://15.185.120.84';
  static String exportPDFUrl = 'http://15.185.120.84/admin/exportsurvey/';

  static String login = 'user/login'; //POST
  static String forgotPassword = 'user/forgot-password'; //POST
  static String changePassword = 'user/change-password'; //POST
  static String logout = 'user/logout'; //POST

//  static String getDepartments = 'department?page='; //POST
  static String getDepartments = 'department/get-department_new'; //GET
  static String getDepartmentInfo = 'department/option/'; //POST

  static String getSurvey = 'survey'; //POST
  static String guidelines = 'survey/guidelines'; //POST

  static String getMarketPractices = 'department/get-bestpractices?page='; //POST
  static String addMarketPractices = 'survey/addbestpractices'; //POST
}
