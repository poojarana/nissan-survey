import 'package:nissan_survey/src/network/NetworkConstants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class NetworkApiUtil {
  static final int timeout = 88;
  static final apiKey = "9?]_T-#:~Gr<?L.z88";
  static final JsonDecoder _decoder = new JsonDecoder();

  static Future<dynamic> getRequest(String url, String auth) {
    return http.get(url, headers: <String, String>{
      'X-API-KEY': apiKey,
      'x-access-token': auth
    }).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("statusCode = $statusCode");
      print(res);

      dynamic parsed = getParsedResponse(res);
      return parsed;
    });
  }

  static Future<dynamic> postRequest(String url, Map param, String auth) {
    return http
        .post(url,
            headers: <String, String>{
              'X-API-KEY': apiKey,
              'x-access-token': auth,
              'content-type': 'application/json'
            },
            body: jsonEncode(param))
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("statusCode = $statusCode");
      print(res);

      dynamic parsed = getParsedResponse(res);
      return parsed;
    });
  }

  static Future<dynamic> putRequest(String url, String auth) {
    return http.put(
      url,
      headers: <String, String>{
        'X-API-KEY': apiKey,
        "x-access-token": auth,
        'content-type': 'application/json'
      },
    ).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("statusCode = $statusCode");
      print(res);

      dynamic parsed = getParsedResponse(res);
      return parsed;
    });
  }

  static Future<String> downloadPDF(String surveyId) async {
    return http
        .get(ApiConstants.exportPDFUrl + surveyId)
        .then((http.Response response) async {
      var bytes = response.bodyBytes;

      if (Platform.isAndroid) {
        Map<Permission, PermissionStatus> statuses = await [
          Permission.storage,
        ].request();

        if (statuses[Permission.storage] == PermissionStatus.granted) {
          final Directory downloadDirectory = Directory('/sdcard/download/');
          File file = File('${downloadDirectory.path}/$surveyId.pdf');
          await file.writeAsBytes(bytes);
          return file.path;
        } else {
          return null;
        }
      } else {
        Directory directory = await getApplicationDocumentsDirectory();
        File file = File('${directory.path}/$surveyId.pdf');
        await file.writeAsBytes(bytes);
        return file.path;
      }
    });
  }

  static dynamic getParsedResponse(String res) {
    try {
      dynamic parsed = _decoder.convert(res);
      if (parsed.isEmpty) {
        return {"error": true, "message": "Internal server error"};
      } else {
        return parsed;
      }
    } catch (e) {
      return {"error": true, "message": e.toString()};
    }
  }
}
