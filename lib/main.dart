import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nissan_survey/src/screens/Login/SplashScreen.dart';
import 'package:nissan_survey/src/utils/AppColors.dart';
import 'package:nissan_survey/src/utils/AppConstants.dart';
import 'package:nissan_survey/src/utils/SharedPrefs.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppConstants.isLoggedIn = await SharedPrefs.isUserLoggedIn();
  runApp(NissanSurveyApp());
}

class NissanSurveyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: ThemeData(fontFamily: Platform.isIOS ? "Roboto" : "DIN", // for iOS
          cupertinoOverrideTheme: CupertinoThemeData(
            primaryColor: AppColors.labelGrayColor,
          ),
          // for others(Android, Fuchsia)
          cursorColor: AppColors.labelGrayColor),
    );
  }
}
